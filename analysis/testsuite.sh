#!/bin/bash
cat datalists/all_exp1.txt | grep "Subject 0" > test_exp1.txt
cat datalists/all_exp2.txt | grep "Subject 0" > test_exp2.txt


echo "TESTING ADJACENT"
python analysis.py -i test_exp2.txt adjacent; xpdf Adjacent_output.pdf&
echo "TESTING SUMMARISE"
python analysis.py -i test_exp1.txt summarise; cat summary.csv | head -5
echo "TESTING ANALYSE"
python analysis.py -i datalists/attr_hist.txt analyse -c Both | head -5
echo "TESTING HISTS"
python analysis.py -i datalists/attr_hist.txt hists -f exp1-attribute-dists.pdf | head -5; xpdf 1exp1-attribute-dists.pdf &
echo "TESTING GLOBAL"
python analysis.py -i test_exp1.txt global | head -5
echo "TESTING NUMDROPS"
python analysis.py -i test_exp1.txt numdrops -f ndtest1.pdf | head -5; xpdf ndtest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Greyed,Display.Absent numdrops -f ndtest2.pdf | head -5; xpdf ndtest2.pdf&
echo "TESTING MAKEFILTER"
python analysis.py -i test_exp2.txt -s Display.Absent makefilter -c Order | head -5
echo "TESTING ACCGRAPH"
python analysis.py -i test_exp1.txt accgraph -c 80 -f agtest1.pdf | head -5; xpdf agtest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed accgraph -c 80 -f agtest2.pdf | head -5; xpdf agtest2.pdf&
echo "TESTING MICFACET"
python analysis.py -i test_exp1.txt micfacet -c 2 -o paper -f mftest1.pdf | head -5; xpdf mftest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed micfacet -c 2 -o paper -f mftest2.pdf | head -5; xpdf Greyedmftest2.pdf&; xpdf Absentmftest2.pdf&
echo "TESTING RFORMAT"
python analysis.py -i test_exp1.txt rformat -o test.csv | head -5
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed rformat -o test.csv | head -5
echo "TESTING SURFACET"
python analysis.py -i test_exp1.txt surfacet -f sftest1.pdf | head -5; xpdf sftest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed surfacet -f sftest2.pdf | head -5; xpdf Greyedsftest2.pdf&; xpdf Absentsftest2.pdf&
echo "TESTING AGGSURF"
python analysis.py -i test_exp1.txt aggsurf -f astest1.pdf | head -5; xpdf astest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed aggsurf -f astest2.pdf | head -5; xpdf astest2.pdf&
echo "TESTING CATBALANCE"
python analysis.py -i test_exp1.txt catbalance --factors AcceptRejectFocus,ResponseCounterbalancing,PriceRatingOrder -f cbtest1.pdf | head -5; xpdf cbtest1.pdf&
python analysis.py -i test_exp2.txt -s Display.Absent,Display.Greyed catbalance --factors AcceptRejectFocus,GreyedItemDisplay,PriceRatingOrder -f cbtest2.pdf | head -5; xpdf cbtest2.pdf&

sleep 1


rm test_exp1.txt test_exp2.txt summary.csv Adjacent_output.pdf 1exp1-attribute-dists.pdf ndtest1.pdf ndtest2.pdf Absentmftest2.pdf Absentsftest2.pdf agtest1.pdf agtest2.pdf astest1.pdf astest2.pdf cbtest1.pdf cbtest2.pdf Greyedmftest2.pdf Greyedsftest2.pdf mftest1.pdf sftest1.pdf

