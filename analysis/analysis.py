#!/usr/bin/env python
import functools
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter, FuncFormatter
import click
import subprocess
import os
from itertools import zip_longest
from collections import defaultdict, OrderedDict
import analysis_settings as aset
import analysis_helpers as ahelp
import analysis_plotters as aplot
with ahelp.suppress_stdout():
    import settings as sett
    import calculations as calc
    from expyriment.misc.data_preprocessing import read_datafile
    import rpy2.robjects as robjects
    from rpy2.robjects.packages import importr
stats = importr('stats')

sns.set_style("white")


ksscale = [(.05, '**'), (.15, '*')]


def ksswitch(ks_str):
    """Switch the comparison operator in a KS string repr"""
    if '<' in ks_str:
        return ks_str.replace('<', '>')
    return ks_str.replace('>', '<')


def ratescale(val, scale=[(.01, '**'), (.05, '*'), (.1, '†')]):
    """rate the pvalue for MIC in terms of *'s"""
    assert all(s < t for s, t in zip(scale, scale[1:]))
    for cval, rating in scale:
        if val < cval:
            return rating
    return None


def mm2inch(*tupl):
    """Convert mm to inches for matplotlib figsize"""
    inch = 25.4
    if len(tupl) == 1:
        return tupl[0] / inch
    return tuple(i / inch for i in tupl)


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def eta_squared(aov):
    aov['eta_sq'] = 'NaN'
    aov['eta_sq'] = aov[:-1]['sum_sq'] / sum(aov['sum_sq'])
    return aov


def retrieve_data(filename):
    '''Get the subject_info and a Dataframe of the raw data for this file'''
    data, headers, subject_info, cmts = read_datafile(filename)
    df = pd.DataFrame(data, columns=headers)
    df['RT'] = pd.to_numeric(df['RT'], errors='coerce')
    df['Correct'] = df['Correct'].map(aset.correct_map)
    df = df[df.BlockName != 'Practice Block']
    return clean_info(subject_info), df


def categorise(df):
    """Collapse Salience levels to catgory column"""
    category = df.PriceSalience + df.RatingSalience
    category = category.map(aset.category_shortener)
    return df.assign(Category=category)


def get_correct(dataframe):
    """get only correct responses"""
    correct_only = dataframe[dataframe['Correct'] == 1]
    correct_only = correct_only.drop('Correct', axis=1)
    return correct_only


def accuracy_summary(df):
    """Get accuracy for certain categories of trials"""
    summ = []
    pmatch = df.PriceSalience.isin(['High', 'Low'])
    rmatch = df.RatingSalience.isin(['High', 'Low'])
    psingle = pmatch & (df.RatingSalience == 'OutOfBounds')
    rsingle = rmatch & (df.PriceSalience == 'OutOfBounds')
    summ.append({'Category': 'DoubleTarget',
                 'Accuracy': df[pmatch & rmatch].Correct.mean()})
    summ.append({'Category': 'PriceSingleTarget',
                 'Accuracy': df[psingle].Correct.mean()})
    summ.append({'Category': 'RatingSingleTarget',
                 'Accuracy': df[rsingle].Correct.mean()})
    summ.append({'Category': 'DoubleDistractor',
                 'Accuracy': df[~(pmatch | rmatch)].Correct.mean()})

    return pd.DataFrame(summ, columns=['Category', 'Accuracy']).round(2).set_index('Category')


def clean_means(df):
    """reorder rows and clean dataframe for means"""
    df.rename({'RT': 'meanRT'}, axis=1, inplace=True)
    order = 'HH;HL;LH;LL;HO;OH;LO;OL;OO'.split(';')
    df.columns = df.columns.get_level_values(0)
    return df.reindex(order).rename_axis(None)


def correct_means(df):
    '''Restrict dataframe to correct only results, add quantiles'''
    group = df.groupby(['Category'])
    cmeans = group.mean()
    cmeans['q.05'] = group.quantile(0.05)
    cmeans['q.25'] = group.quantile(0.25)
    cmeans['q.50'] = group.quantile(0.50)
    cmeans['q.75'] = group.quantile(0.75)
    cmeans['q.95'] = group.quantile(0.95)
    return clean_means(cmeans)


def split_means(df):
    '''Restrict dataframe to correct only results, add quantiles'''
    cmeans = df.groupby(['Category']).mean()
    return clean_means(cmeans)


def apply_split(df, func):
    """Create a <func> DF split by Display(simple-symbolic)"""
    greyed = func(df[df.Display == 'Greyed'])
    absent = func(df[df.Display == 'Absent'])
    return pd.concat([greyed, absent], axis=1, keys=['Greyed', 'Absent'])


def clean_data(df, clean='Both', split=None):
    """Clean one or both of the ends of each catgorical distribution"""
    if split:
        col, val = split.split('.')
        df = df[df[col] == val]
    if clean == 'None':
        return df
    notLowRT = df.RT > 300
    notHighRT = df.RT < df.RT.quantile(.95)
    isANumber = ~pd.isna(df.RT)
    if clean == 'Low':
        return df[notLowRT]
    if clean == 'High':
        return df[notHighRT]
    return df[notHighRT & notLowRT & isANumber]


def clean_info(info):
    """clean weird bytes marks from string keys/vals in info dict"""
    new_dict = {}
    for k, v in info.items():
        if k.startswith("b'"):
            new_dict[k[2:-1]] = v[2:-1]
        else:
            new_dict[k] = v
    if new_dict['id'] == '76738':
        new_dict['id'] = '1'
    return new_dict


def four_quadrants(correct_data):
    """return a dataframe with only High and Low saliency rows"""
    pricefilter = correct_data['PriceSalience'] != 'OutOfBounds'
    ratefilter = correct_data['RatingSalience'] != 'OutOfBounds'
    noout = correct_data[pricefilter & ratefilter]
    return categorise(noout)


def getminmax(list_of_files, split):
    """get minimum and maximum RT over all files in list for main four categories"""
    minRT, maxRT = None, None
    for filename in list_of_files:
        _, fourq = process_file(filename, split=split)
        minRT = min(fourq.RT) if minRT is None else min(min(fourq.RT), minRT)
        maxRT = max(fourq.RT) if maxRT is None else max(max(fourq.RT), maxRT)
    return minRT, maxRT


def run_kstests(df):
    """run kstests on each pair of the four quadrants
    kstests match the sft R package siDominance test with the added test of HH>LL"""
    quads = zip(['HH'] * 5 + ['HL'] * 2 + ['LH'] * 2,
                ['LL'] + ['HL'] * 2 + ['LH'] * 2 + ['LL'] * 4,
                ['greater'] + ['greater', 'less'] * 4,
                ['Good'] + ['Good', 'Bad'] * 4)

    cmap = {'greater': ' > ', 'less': ' < '}
    ksstats = []
    for first_cat, second_cat, comparison, desired in quads:
        first_arr = robjects.FloatVector(df[df['Category'] == first_cat]['RT'])
        second_arr = robjects.FloatVector(df[df['Category'] == second_cat]['RT'])
        ksresults = stats.ks_test(first_arr, second_arr, alternative=comparison, exact=False)
        ksstats.append({'Comparison': first_cat + cmap[comparison] + second_cat,
                        'KS_Stat': ksresults.rx('statistic')[0][0],
                        'Pvalue': ksresults.rx('p.value')[0][0],
                        'SigP_Desired': desired})

    return pd.DataFrame(ksstats)


def create_latex(fig, axes, info, section=aset.section_latex):
    '''Create a latex section and convert to pdf for the figures and info'''
    for k, v in info.items():
        if type(v) == pd.DataFrame:
            info[k] = v.to_latex()
    if fig is not None:
        figname = 'temp/{}figure.png'.format(info['id'])
        fig.savefig(figname)
        info['figure'] = '{}figure'.format(info['id'])
    else:
        info['figure'] = '1x1.png'
    return section.format(**info)


def anova(fourq):
    """Create an anova table for four way interaction"""
    formula = 'RT ~ PriceSalience * RatingSalience'
    model = sm.formula.ols(formula, fourq).fit()
    aov_table = sm.stats.anova_lm(model, typ=2)
    return eta_squared(aov_table).round(4)


@functools.lru_cache()
def process_file(datafile, clean='Both', split=None):
    '''Run the processing on one file'''
    info, df = retrieve_data(datafile)
    df = clean_data(df, clean, split)
    info['accuracy'] = accuracy_summary(df).rename_axis(None)
    conly = get_correct(df)
    cmeans = correct_means(categorise(conly))
    cmeans.columns = cmeans.columns.get_level_values(0)
    cmeans = cmeans.rename_axis(None)
    info['cmeans'] = cmeans.round(0)
    info['MIC'] = ahelp.MIC(cmeans)
    fourq = four_quadrants(conly)
    info['Anova'] = anova(fourq).round(3)
    info['ksstat'] = run_kstests(fourq).round(3)
    if 'ResponseCounterbalancing' not in info:
        info['ResponseCounterbalancing'] = 'RIGHT (/)'
    return info, fourq


def summarise_file(info, fourq):
    """Extract summarised stats for one files data"""
    details = dict.fromkeys(aset.colorder)
    details['SubjectID'] = info['id']
    details['Focus'] = info['AcceptRejectFocus'][0]
    details['AcceptHand'] = info['ResponseCounterbalancing'][0]
    details['FirstAttr'] = info['PriceRatingOrder'][0]
    acc = info['accuracy'].Accuracy.rename(index=lambda x: x + 'Acc')
    details.update(acc.to_dict())
    anova = info['Anova'].rename(columns={'PR(>F)': 'P'}).drop(columns=['sum_sq', 'df'])
    anova = anova.rename(index=lambda x: x.replace('Salience', '').replace(':', 'x') + '_Anova_')
    anova = anova.stack()
    anova.index = anova.index.map(''.join)
    details.update(anova.to_dict())
    details['MIC'] = info['MIC']
    mRTs = info['cmeans'].meanRT
    mRTs = mRTs.drop(index=[x for x in mRTs.index if 'O' in x])
    details.update(mRTs.rename(index=lambda x: x + '_MeanRT').to_dict())
    return details


@click.group()
@click.version_option(version='0.2.0')
@click.option('--infiles', '-i', type=click.Path(exists=True, file_okay=True, resolve_path=True), help='File containing datafiles to process, one file path per line', multiple=True, required=True)
@click.option('--splits', '-s', help='Multiple splits of input file(s) on column value, applies to all file paths', default='')
@click.pass_context
def main(ctx, infiles, splits):
    """The SFT Python Analysis Script"""
    splitdict = _parse_splits(splits)
    files = []

    for f in infiles:
        with open(f) as infile:
            files += infile.read().splitlines()
    ctx.obj = ahelp.InputSpec(splitdict, files)


@main.command(name='adjacent')
@ahelp.pass_input
def adjacent(spec):
    """Run a side by side analysis of the Simple Symbolic datasets"""
    sections = []
    for datafile in spec.files:
        info, df = retrieve_data(datafile)
        df = clean_data(df)
        acc = apply_split(df, accuracy_summary)
        acc.columns = acc.columns.get_level_values(0)
        info['accuracy'] = acc.rename_axis(None)
        conly = get_correct(df)
        cmeans = apply_split(categorise(conly), split_means)
        cmeans.columns = cmeans.columns.get_level_values(0)
        info['cmeans'] = cmeans.round(0)
        g_mic = ahelp.MIC(cmeans, colname='Greyed')
        a_mic = ahelp.MIC(cmeans, colname='Absent')
        fourq = four_quadrants(conly)
        g_anova = anova(fourq[fourq.Display == 'Greyed'])
        a_anova = anova(fourq[fourq.Display == 'Absent'])
        base = 'Greyed ANOVA (MIC: {:.2f})\\\\{}\\\\Absent ANOVA (MIC: {:.2f})\\\\{}'
        info['Anova'] = base.format(g_mic, g_anova.round(2).to_latex(),
                                    a_mic, a_anova.round(2).to_latex())
        if 'ResponseCounterbalancing' not in info:
            info['ResponseCounterbalancing'] = 'RIGHT (/)'
        fig, axes = aplot.plot_adjacent(fourq, info)
        sections.append(create_latex(fig, axes, info, section=aset.adjacent_section_latex))
    with open('temp/output.tex', 'w') as latexfile:
        output_text = aset.latex_report.format(sections='\n'.join(sections))
        latexfile.write(output_text.replace('>', '$>$').replace('<', '$<$'))
        latexfile.write('\n')
    os.chdir('temp')
    subprocess.run(['pdflatex', 'output.tex'], stdout=subprocess.DEVNULL)
    os.rename('output.pdf', '../{}output.pdf'.format('Adjacent_'))


@main.command(name='summarise')
@ahelp.pass_input
def summarise(spec):
    '''Get summary statistics into a csv file'''
    summdata = []
    for datafile in spec.files:
        info, fourq = process_file(datafile)
        summdata.append(summarise_file(info, fourq))

    summdata = pd.DataFrame(summdata)[aset.colorder]
    summdata.round(2).to_csv('summary.csv')


@main.command(name='analyse')
@click.option('--pdf', '-p', is_flag=True, help='Print output to latex file')
@click.option('--graph', '-g', is_flag=True, help='Show graphs or write to pdf')
@click.option('--double', '-d', is_flag=True, default=False, help='Graphs only have MIC/SIC')
@click.option('--single', is_flag=True, default=False, help='Graphs only SIC')
@click.option('--stderr', '-s', is_flag=True, default=False, help='Show SIC graph with stderr')
@click.option('--clean', '-c', type=click.Choice(['None', 'Low', 'High', 'Both']), help='Remove RT\'s from ends of the categorical distributions')
@ahelp.pass_input
def analyse_files(spec, pdf, graph, double, single, stderr, clean):
    """For each file in files run the basic analyses"""
    if pdf:
        sections = []
    for split, datafile in spec.split_file_tuples():
        info, fourq = process_file(datafile, clean=clean, split=split)
        if graph:
            if single:
                fig, axes = aplot.plot_SICfromfourq(fourq, info, showstd=stderr)
            elif double:
                fig, axes = aplot.plot_twoways(fourq, info, showstd=stderr)
            else:
                fig, axes = aplot.plot_fourways(fourq, info, showstd=stderr)
        else:
            fig, axes = None, None
        if pdf:
            sections.append(create_latex(fig, axes, info))
            plt.close(fig)
            continue
        print(aset.report.format(**info))
        plt.show()
    if pdf:
        with open('temp/output.tex', 'w') as latexfile:
            output_text = aset.latex_report.format(sections='\n'.join(sections))
            latexfile.write(output_text.replace('>', '$>$').replace('<', '$<$'))
            latexfile.write('\n')
        os.chdir('temp')
        subprocess.call(['pdflatex', 'output.tex'])
        os.rename('output.pdf', '../{}output.pdf'.format(split))


@main.command(name='hists')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@ahelp.pass_input
def create_hists(spec, figfile):
    """For each file create a histogram of certain feature"""
    for datafile in spec.files:
        info, df = retrieve_data(datafile)
        accept_file = 'Accept' in info['AcceptRejectFocus']
        if accept_file:
            pdists, rdists = sett.accept_price, sett.accept_rate
        else:
            pdists, rdists = sett.reject_price, sett.reject_rate

        df.Price = df.Price.str[1:]
        df.Rating = df.Rating.str[:-1]
        df.Price = pd.to_numeric(df.Price, errors='raise')
        df.Rating = pd.to_numeric(df.Rating, errors='raise')
        df = df.replace(to_replace={'OutOfBounds': 'Distractor'})
        if figfile:
            plt.figure(figsize=mm2inch(190, 100), dpi=300)
        with sns.color_palette(n_colors=3):
            fig, axes = plt.subplots(ncols=1, nrows=2)
        attributes = ('Price', 'Rating')
        criteria = (500, 50)
        dists = (pdists, rdists)
        formats = ('${x:.0f}', '{x:.0f}%')
        legends = ((0.57, 1), (0.15, 1)) if accept_file else ((0.15, 1), (0.57, 1))
        for ax, col, crit, dist, fmt, leg in zip(axes, attributes, criteria, dists, formats, legends):
            ax.xaxis.set_major_formatter(StrMethodFormatter(fmt))
            salience_col = col + 'Salience'
            for salience in ('High', 'Low', 'Distractor'):
                series = df[df[salience_col] == salience][col]
                bins = np.linspace(series.min(), series.max(), 12)
                sns.distplot(series, bins=bins, label=salience, ax=ax, kde=False, norm_hist=True)
            ax.axvline(crit, color='r', label='Criteria')
            for key in ('highab', 'lowab', 'outab'):
                tdist = dist[key]
                if type(tdist) == tuple:
                    tmin, tmax, tmu, tsd = tdist
                    ta, tb = calc.spec_truncnorm(*tdist)
                    ftrunc = calc.truncnorm(ta, tb, loc=tmu, scale=tsd)
                    x = np.linspace(tmin, tmax, 100)
                    ax.plot(x, ftrunc.pdf(x))
            ax.legend(loc='upper left', bbox_to_anchor=leg)
            ax.set_ylabel('Density')

        plt.tight_layout()
        sns.despine()
        if figfile:
            plt.savefig(info['id'] + figfile)
        else:
            plt.show()


@main.command(name='global')
@ahelp.pass_input
def global_view(spec):
    """Run an analysis on all participants grouped together"""
    df_all = pd.DataFrame()
    all_info = []
    for datafile in spec.files:
        info, df = retrieve_data(datafile)
        df = clean_data(df)
        all_info.append(info)
        df_all = pd.concat([df_all, df], ignore_index=True)
    conly = get_correct(df_all)
    cmeans = correct_means(categorise(conly))
    fourq = four_quadrants(conly)
    print(cmeans)
    print(fourq)


def _accgraphworker(files, criteria, dfs, keys, filepaths, split):
    """Do the grunt work for the accuracy graph maker"""
    for datafile in files:
        info, df = retrieve_data(datafile)
        df = clean_data(df, split=split)
        acc = accuracy_summary(df)
        acc['MinAcc'] = min(acc.Accuracy)
        dfs.append(acc)
        sid = info['id'].zfill(2)
        keys.append(sid)
        filepaths[sid] = datafile
    all_acc = pd.concat(dfs, keys=keys)
    all_acc = all_acc.reset_index().rename(columns={'level_0': 'SubjectID'})
    all_acc = all_acc.sort_values(by='MinAcc')
    order = all_acc.SubjectID.unique()
    cutoff = len(all_acc[all_acc.MinAcc < criteria]) / 4
    return all_acc, order, cutoff


@main.command(name='numdrops')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@ahelp.pass_input
def show_drop_stats(spec, figfile):
    """Show the mean and sd of the number of dropped trials"""
    split_tuples = spec.splits.items() if spec.splits else [(None, None)]
    for key, split in split_tuples:
        counts = []
        for datafile in spec.files:
            info, df = retrieve_data(datafile)
            df = clean_data(df, split=split, clean='None')
            cdf = clean_data(df, split=split)
            counts.append({'sid': info['id'], 'raw': len(df),
                           'clean': len(cdf), 'nan': len(df[df.RT.isnull()]),
                           'fast': len(df[df.RT < 300]),
                           'slow': len(df[df.RT > df.RT.quantile(.95)])})
        counts = pd.DataFrame(counts)
        counts['diffs'] = counts.raw - counts.clean
        aplot.plot_dropped(counts, len(df))
        if figfile:
            plt.savefig(figfile)
        else:
            plt.show()
        print(split)
        print(counts[counts.diffs > 63])
        print('Mean dropped is {} and SD is {}'.format(counts.diffs.mean(), counts.diffs.std()))
        percents = [x / counts.raw.mean() for x in [counts.fast.mean(), counts.slow.mean(), counts.nan.mean()]]
        print('Mean percentages of Fast: {:.2}, Slow: {:.2} and Nan: {:.2}'.format(*percents))


def _parse_splits(splits):
    """Make a dictionary of splits and return it"""
    if not splits:
        return None
    splitdict = OrderedDict()
    for split in splits.split(','):
        c, key = split.split('.')
        splitdict[key] = split
    return splitdict


@main.command(name='makefilter')
@click.option('--criteria', '-c',
              type=click.Choice(['Order', 'RelaxedOrder', 'NoViolations', 'Violations', 'File', 'DistOrder', 'SomeOrder']),
              help='Make a filter based on criteria')
@click.option('--filterfile', type=click.Path(), help='Filter out participant not in file')
@ahelp.pass_input
def make_filter(spec, criteria, filterfile):
    """Make a filter list based on a criteria

    Criteria can be one of:
        Order:- Make filter list based on ordering of mean RT in four quadrants"""

    outlist = []
    if criteria == 'File':
        if not filterfile:
            raise click.BadOptionUsage(criteria, 'filterfile must be provided if filtering by File')
        with open(filterfile) as ffile:
            dropfiles = ffile.read().splitlines()
            outlist = [f for f in spec.files if f not in dropfiles]
        print('\n'.join(outlist))
        return

    for split, datafile in spec.split_file_tuples():
        info, fourq = process_file(datafile, split=split)
        if criteria == 'Order':
            mHH = fourq[fourq.Category == 'HH'].RT.mean()
            mHL = fourq[fourq.Category == 'HL'].RT.mean()
            mLH = fourq[fourq.Category == 'LH'].RT.mean()
            mLL = fourq[fourq.Category == 'LL'].RT.mean()
            if mHH < mHL and mHH < mLH:
                if mHL < mLL and mLH < mLL:
                    outlist.append(datafile)
        elif criteria == 'RelaxedOrder':
            mHH = fourq[fourq.Category == 'HH'].RT.mean()
            mHL = fourq[fourq.Category == 'HL'].RT.mean()
            mLH = fourq[fourq.Category == 'LH'].RT.mean()
            mLL = fourq[fourq.Category == 'LL'].RT.mean()
            HLHH = mHL - mHH
            LHHH = mLH - mHH
            LLHL = mLL - mHL
            LLLH = mLL - mLH
            for comparison in [HLHH, LHHH, LLHL, LLLH]:
                if comparison < -10:
                    break
            else:
                outlist.append(datafile)
        elif criteria == 'NoViolations':
            kss = info['ksstat']
            kssbad = kss[kss.SigP_Desired == 'Bad']
            if not (kssbad.Pvalue < 0.15).any(axis=None):
                outlist.append(datafile)
        elif criteria == 'Violations':
            kss = info['ksstat']
            kssbad = kss[kss.SigP_Desired == 'Bad']
            if (kssbad.Pvalue < 0.15).any(axis=None):
                outlist.append(datafile)
        elif criteria == 'DistOrder':
            kss = info['ksstat']
            kssbad = kss[kss.SigP_Desired == 'Bad']
            kssgood = kss[kss.SigP_Desired == 'Good']
            if not (kssbad.Pvalue < 0.15).any(axis=None):
                if (kssgood.Pvalue < 0.15).all(axis=None):
                    outlist.append(datafile)
        elif criteria == 'SomeOrder':
            kss = info['ksstat']
            kssbad = kss[kss.SigP_Desired == 'Bad']
            kssgood = kss[kss.SigP_Desired == 'Good']
            if not (kssbad.Pvalue < 0.15).any(axis=None):
                if (kssgood.Pvalue < 0.15).any(axis=None):
                    outlist.append(datafile)

    print('\n'.join(outlist))


@main.command(name='accgraph')
@click.option('--output', '-o', type=click.Path(file_okay=True), help='Send list of subjects that meet criteria to file, ordered via accuracy')
@click.option('--criteria', '-c', type=click.IntRange(0, 100), default=0, help='A percentage for which the criteria of minimum accuracy is set')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@ahelp.pass_input
def create_accuracy_graph(spec, output, criteria, figfile):
    """Create an accuracy graph across all participants for each of:
        Double Target
        SingleTargetPrice
        SingleTargetRating
        DoubleDistractor"""
    dfs = defaultdict(list)
    keys = defaultdict(list)
    filepaths = defaultdict(dict)
    all_acc, order, cutoff = dict(), dict(), dict()
    criteria, crit_percent = criteria / 100.0, criteria
    kwargs = {}
    if figfile:
        kwargs['figsize'] = mm2inch(190, 100 * spec.num_splits)
        kwargs['dpi'] = 300
    for key, split, files in spec.key_split_filelist_tuples():
        all_acc[key], order[key], cutoff[key] = _accgraphworker(files, criteria, dfs[key], keys[key], filepaths[key], split)
    with sns.axes_style('ticks'):
        fig, axes = plt.subplots(nrows=spec.num_splits, **kwargs)
        for key, ax in zip(sorted(all_acc.keys()), fig.axes):
                sns.pointplot(x="SubjectID", y="Accuracy", hue="Category",
                              data=all_acc[key], order=order[key], scale=0.3,
                              ax=ax, hue_order=['DoubleTarget',
                                                'PriceSingleTarget',
                                                'RatingSingleTarget',
                                                'DoubleDistractor'])
                ax.set_title(aset.titleMap[key])
                ax.set_ylim(-0.05, 1.05, auto=True)
                ax.set_xticklabels([])
                ax.set_xlabel('Participants')
                ax.axvline(cutoff[key] - 0.5, label='{}% cutoff'.format(crit_percent), color='r')
                ax.legend()
    plt.tight_layout()
    if figfile:
        plt.savefig(figfile)
    else:
        plt.show()
    if output:
        for s in filepaths:
            lines = '\n'.join(filepaths[s][sid] for sid in order[s][int(cutoff[s]):])
            if s:
                root, ext = os.path.splitext(output)
                newoutput = root + '_' + s.lower() + ext
            else:
                newoutput = output
            with open(newoutput, 'w') as outfile:
                outfile.write(lines)
                outfile.write('\n')


def factorMIC(files, figfile=None, split=None, filters=None, columns=1, title='', papersize='paper', legendplace=(1.5, 0.9)):
    """run a factorplot for just these files"""
    dfs, keys, pvals, filepaths, bgs = [], [], {}, {}, {}
    sz = 5
    cwrap = columns
    aspect = 1.2
    size_mult = 1
    if papersize == 'poster':
        size_mult = 2
    if figfile:
        plt.figure(dpi=300)
        sz = mm2inch(((190 * size_mult) / cwrap) / aspect)

    for datafile in files:
        if datafile is None:
            continue
        info, fourq = process_file(datafile, split=split)
        dfs.append(fourq)
        sid = info['id'].zfill(2)
        pvals[sid] = [p for p in anova(fourq)['PR(>F)'][:-1]]
        keys.append(sid)
        filepaths[sid] = datafile
        bgs[sid] = None
        if filters and datafile in filters:
            bgs[sid] = 'xkcd:maize'

    ldf = len(dfs)
    all_four_p1 = pd.concat(dfs[:ldf // 2], keys=keys[:ldf // 2], names=['SubjectID'])
    all_four_p2 = pd.concat(dfs[ldf // 2:], keys=keys[ldf // 2:], names=['SubjectID'])

    all_four = pd.concat([all_four_p1, all_four_p2])
    all_four = all_four.reset_index()
    keys.sort()

    with sns.plotting_context(context=papersize):
        g = sns.factorplot(data=all_four, col="SubjectID", size=sz, col_wrap=cwrap,
                           x='PriceSalience', y='RT', hue='RatingSalience',
                           markers=["o", "x"], linestyles=["-", "--"],
                           order=['Low', 'High'], hue_order=['Low', 'High'],
                           dodge=True, sharey=False, aspect=aspect, scale=0.6,
                           col_order=keys, legend=False)
        g.set_titles("{col_name}")

        for sid, ax in zip(keys, g.axes):
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, p: '{x:.2f}'.format(x=x / 1000.0)))
            txtbits = [(l, ratescale(p)) for l, p in zip(['p', 'r', 'p.r'], pvals[sid])]
            txtbits = [''.join(t) for t in txtbits if t[1]]
            ax.text(1, 0.5, '\n'.join(txtbits), horizontalalignment='center',
                    verticalalignment='center', transform=ax.transAxes,
                    style='italic', size='x-small')
            if bgs[sid]:
                ax.set_facecolor(bgs[sid])
        plt.legend(bbox_to_anchor=legendplace, loc=2, borderaxespad=0.1, title='Rating Salience', edgecolor='black', frameon=True)
        plt.suptitle(aset.titleMap[title], fontsize=16)
        if figfile:
            plt.savefig(figfile)
        else:
            plt.show()


@main.command(name='micfacet')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@click.option('--filtered', type=click.Path(exists=True, file_okay=True, resolve_path=True), help='File containing subjects to highlight')
@click.option('--columns', '-c', help='Number of columns in the resulting graph', default=4)
@click.option('--outformat', '-o', type=click.Choice(['paper', 'poster']), help='Output format for the figure', default='paper')
@ahelp.pass_input
def create_MIC_facets(spec, figfile, filtered, columns, outformat):
    """Create a facet grid of MIC based on files input to function

    If infile is specified multiple times it must match the order, length and values of splits"""
    if filtered:
        with open(filtered) as ffile:
            filtered = ffile.read().splitlines()
    for key, split, filelist in spec.key_split_filelist_tuples():
        MICargs = dict(filters=filtered, columns=columns, title=key, papersize=outformat)
        MICargs['figfile'] = (ahelp.xstr(key) + figfile) if figfile else figfile
        factorMIC(filelist, split=split, **MICargs)


@main.command(name='rformat')
@click.option('--outfile', '-o', type=click.Path(file_okay=True, resolve_path=True), help='File containing the R package sft formatted csv', required=True)
@ahelp.pass_input
def create_R_formatted_data(spec, outfile):
    """From a list of input files create a csv"""
    dfs = []
    for split, datafile in spec.split_file_tuples():
        info, df = retrieve_data(datafile)
        df = clean_data(df, 'Both', split)
        dfs.append(df)
    all_dfs = pd.concat(dfs)
    all_dfs = all_dfs.reset_index()
    rcols = {'subject_id': 'Subject', 'Display': 'Condition',
             'PriceSalience': 'Channel1', 'RatingSalience': 'Channel2'}
    dcols = ['index', 'BlockName', 'TrialID', 'Price', 'Rating', 'Button', 'Timestamp']
    if 'Display' not in all_dfs.columns:
        all_dfs['Condition'] = 'None'
    all_dfs = all_dfs.rename(columns=rcols)
    all_dfs = all_dfs.drop(columns=dcols, errors='ignore')
    channelmap = {'High': 2, 'Low': 1, 'OutOfBounds': 0}
    all_dfs = all_dfs.replace({'Channel1': channelmap, 'Channel2': channelmap})
    all_dfs.to_csv(outfile)


@main.command(name='plotsic')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@click.option('--columns', '-c', help='Number of columns in the resulting graph', default=3)
@click.option('--outformat', '-o', type=click.Choice(['paper', 'poster']), help='Output format for the figure', default='paper')
@click.option('--legendplace', '-l', default=None, help='comma separated placement for legend')
@ahelp.pass_input
def create_sic_facets(spec, figfile, columns, outformat, legendplace):
    """Create a facet grid of Survivor Interaction Contrasts (SIC) based on files input to function
    """
    size_mult = 1
    if outformat == 'poster':
        size_mult = 2
    if legendplace:
        places = zip(*[iter(float(v) for v in legendplace.split(','))] * 2)
    else:
        places = aset.places('survivor', spec.num_splits)
    for (key, split, filelist), place in zip(spec.key_split_filelist_tuples(), places):
        rect = [0, 0, 1, 0.96] if key else [0, 0, 1, 1]
        nfiles = len(filelist)
        ncols = columns
        kwargs = {}
        nrows = -(-nfiles // ncols)  # Ceiling division!
        _, maxRTall = getminmax(filelist, split)
        if figfile:
            kwargs['figsize'] = mm2inch(190 * size_mult, 50 * nrows * size_mult)
            kwargs['dpi'] = 300
        with sns.plotting_context(context=outformat):
            fig, axes = plt.subplots(nrows=nrows, ncols=ncols, sharey=True, **kwargs)
            for i, ax in enumerate(fig.axes):
                if i < nfiles:
                    info, fourq = process_file(filelist[i], split=split)
                    sid = info['id'].zfill(2)
                    ax.set_title(sid)
                    F = ahelp.Survivor(fourq, minRT=0, maxRT=maxRTall)
                    aplot.plot_SIC(F, ax, legend=False)
                    ax.xaxis.set_major_formatter(FuncFormatter(lambda x, p: '{x:.1f}'.format(x=x / 1000.0)))
                    if i in range(nfiles - ncols, nfiles):
                        ax.set_xlabel('RT (seconds)')
                    else:
                        ax.set_xticks([], [])
                    if i in range(0, nfiles, ncols):
                        ax.set_ylabel('')
                    stats = ahelp.sic_tests(F)
                    for dstat in stats:
                        dstat.add_alpha(0.33, '*')
                        dstat.add_alpha(0.05, '**')

                    txtbits = [str(t) for t in stats]
                    ax.text(0.5, 0.9, '\n'.join(txtbits), horizontalalignment='center',
                            verticalalignment='center', transform=ax.transAxes,
                            style='italic', size='x-small')
                    sns.despine(ax=ax)
                else:
                    ax.set_axis_off()
            plt.tight_layout(rect=rect)
            plt.subplots_adjust(hspace=0.2, wspace=0.1)
            plt.suptitle(aset.titleMap[key], fontsize=16)
            if figfile:
                plt.savefig(ahelp.xstr(key) + figfile)
            else:
                plt.show()


@main.command(name='kstests')
@click.option('--outformat', type=click.Choice(['infer', 'excel', 'html', 'plain', 'latex']), help='Output format for the figure', default='infer')
@click.option('--output', '-o', type=click.Path(file_okay=True), help='Send list of subjects that meet criteria to file, ordered via accuracy', default='kstests.tex')
@ahelp.pass_input
def create_kstest_table(spec, outformat, output):
    """Create a table of kstests on the survivor functions for each four categories
    """
    kstests = []
    keys = []
    for key, split, filelist in spec.key_split_filelist_tuples():
        for filename in filelist:
            info, _ = process_file(filename, split=split)
            kstests.append(info['ksstat'])
            keys.append(info['id'].zfill(2))
    kstests = pd.concat(kstests, keys=keys, names=['SubjectIDs'])
    kstests['repr'] = kstests[['KS_Stat', 'Pvalue']].apply(lambda x: '{} ({})'.format(x[0], x[1]), axis=1)
    kstests = kstests.set_index(['Comparison'], append=True)
    kstests = kstests.reset_index(level=1, drop=True)
    kstests = kstests.drop(columns=['Pvalue', 'KS_Stat', 'SigP_Desired'])
    kstests = kstests.rename(columns={'repr': 'KS_Stat'})
    kstests = kstests.unstack(level=1).reorder_levels([1, 0], axis=1)
    kstests = kstests.reindex('HH < HL;HH > HL;HH < LH;HH > LH;HL < LL;HL > LL;LH < LL;LH > LL;HH > LL'.split(';'), level=0, axis=1)
    kstests.columns = kstests.columns.droplevel(1)
    kstests = kstests.reset_index().reset_index(level=0, drop=True)
    if outformat == 'infer':
        outformat = ahelp.infer(os.path.splitext(output)[1])
    if outformat == 'excel':
        kstests.to_excel(output, index=False)
    elif outformat == 'html':
        kstests.to_html(output, index=False)
    elif outformat == 'plain':
        with open(output, 'w') as outfile:
            kstests.to_string(outfile, index=False)
    else:
        kstests.to_latex(output, index=False)


@main.command(name='surfacet')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@click.option('--columns', '-c', help='Number of columns in the resulting graph', default=3)
@click.option('--outformat', '-o', type=click.Choice(['paper', 'poster']), help='Output format for the figure', default='paper')
@click.option('--legendplace', '-l', default=None, help='comma separated placement for legend')
@click.option('--legendoff', is_flag=True, help='Remove legend')
@click.option('--title', '-t', default=None, help='Title for the figure')
@ahelp.pass_input
def create_survivor_facets(spec, figfile, columns, outformat, legendplace, legendoff, title):
    """Create a facet grid of survivor functions based on files input to function

    If infile is specified multiple times it must match the order, length and values of splits
    output format can currently vbe one of"""
    size_mult = 1
    if outformat == 'poster':
        size_mult = 2
    if legendplace:
        places = zip(*[iter(float(v) for v in legendplace.split(','))] * 2)
    else:
        places = aset.places('survivor', spec.num_splits)
    for (key, split, filelist), place in zip(spec.key_split_filelist_tuples(), places):
        rect = [0, 0, 1, 0.96] if (key or title) else [0, 0, 1, 1]
        nfiles = len(filelist)
        ncols = columns
        kwargs = {}
        nrows = -(-nfiles // ncols)  # Ceiling division!
        if figfile:
            kwargs['figsize'] = mm2inch(190 * size_mult, 50 * nrows * size_mult)
            kwargs['dpi'] = 300
        with sns.plotting_context(context=outformat):
            fig, axes = plt.subplots(nrows=nrows, ncols=ncols, sharey=True, **kwargs)
            for i, ax in enumerate(fig.axes):
                if i < nfiles:
                    info, fourq = process_file(filelist[i], split=split)
                    sid = info['id'].zfill(2)
                    ax.set_title(sid)
                    aplot.plot_ecdfs(fourq, ax, legend=False)
                    ax.xaxis.set_major_formatter(FuncFormatter(lambda x, p: '{x:.1f}'.format(x=x / 1000.0)))
                    if i in range(nfiles - ncols, nfiles):
                        ax.set_xlabel('RT (seconds)')
                    if i in range(0, nfiles, ncols):
                        ax.set_ylabel('$S(t)$')
                    txtbits = [(ksswitch(l), ratescale(p, scale=ksscale)) for l, p in zip(info['ksstat'].Comparison, info['ksstat'].Pvalue)]

                    txtbits = [''.join(t) for t in txtbits if t[1]]
                    ax.text(0.7, 0.6, '\n'.join(txtbits), horizontalalignment='center',
                            verticalalignment='center', transform=ax.transAxes,
                            style='italic', size='x-small')
                    sns.despine(ax=ax)
                else:
                    ax.set_axis_off()
            plt.tight_layout(rect=rect)
            plt.subplots_adjust(hspace=0.4, wspace=0.1)
            handles, labels = fig.axes[0].get_legend_handles_labels()
            if not legendoff:
                legend = fig.legend(handles, labels, loc='lower right', bbox_to_anchor=place, title='Survivor function\nfor Salience levels', edgecolor='black', frameon=True)
                plt.setp(legend.get_title(), multialignment='center')
            if title:
                plt.suptitle(title, fontsize=16)
            else:
                plt.suptitle(aset.titleMap[key], fontsize=16)
            if figfile:
                plt.savefig(ahelp.xstr(key) + figfile)
            else:
                plt.show()


@main.command(name='aggsurf')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@ahelp.pass_input
def create_aggregate_survivor(spec, figfile):
    """Create a facet grid of survivor functions based on files input to function

    If infile is specified multiple times it must match the order, length and values of splits"""
    kwargs = {}
    labels = ['Exp1', 'Exp2']
    if figfile:
        kwargs['figsize'] = mm2inch(190, 80)
        kwargs['dpi'] = 300
    with sns.plotting_context(context='poster'):
        fig, axes = plt.subplots(nrows=1, ncols=2, sharey=True, **kwargs)
        for i, ax in enumerate(fig.axes):
            all_fours = []
            for f in spec.files:
                info, fourq = process_file(f)
                all_fours.append(fourq)

            aplot.plot_ecdfs(pd.concat(all_fours, ignore_index=True), ax, legend=False)
            ax.xaxis.set_major_formatter(FuncFormatter(lambda x, p: '{x:.1f}'.format(x=x / 1000.0)))
            if i == 0:
                ax.set_xlabel('RT (seconds)')
            ax.set_ylabel('$S(t)$')
            txtbits = [(ksswitch(l), ratescale(p, scale=ksscale)) for l, p in zip(info['ksstat'].Comparison, info['ksstat'].Pvalue)]
            txtbits = [''.join(t) for t in txtbits if t[1]]
            ax.text(0.7, 0.6, '\n'.join(txtbits), horizontalalignment='center',
                    verticalalignment='center', transform=ax.transAxes,
                    style='italic', size='x-small')
            sns.despine(ax=ax)
        plt.tight_layout()
        plt.subplots_adjust(hspace=0.4, wspace=0.1)
        handles, labels = fig.axes[0].get_legend_handles_labels()
        legend = fig.legend(handles, labels, loc='lower right', bbox_to_anchor=(0.5, 0.5), title='Survivor function\nfor Salience levels', edgecolor='black', frameon=True)
        plt.suptitle('Survivor function spread', x=0.85, y=0.3, fontsize=16)
        plt.setp(legend.get_title(), multialignment='center')
        if figfile:
            plt.savefig(figfile)
        else:
            plt.show()


@main.command(name='catbalance')
@click.option('--factors', help='A comma separated list of columns to organise by')
@click.option('--figfile', '-f', type=click.Path(file_okay=True), help='A filename to save the figure as')
@ahelp.pass_input
def category_balance_checker(spec, factors, figfile):
    """Create a display to represent how many participants are in each counterbalanced category."""
    kwargs = {}
    factors = factors.split(',')
    if figfile:
        kwargs['figsize'] = mm2inch(190, 80)
        kwargs['dpi'] = 300
    with sns.plotting_context(context='talk'):
        all_info = []
        for f in spec.files:
            info, _ = retrieve_data(f)
            all_info.append(info)
        df = pd.DataFrame(all_info)[factors]
        df['cats'] = df[factors].apply('_'.join, axis=1)
        ax = sns.countplot(df.cats)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30, ha='right')

        if figfile:
            plt.savefig(figfile)
        else:
            plt.show()


if __name__ == '__main__':
    main()
