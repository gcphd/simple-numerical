#!/bin/bash
COMBINE=helper_scripts/combine.py
COMBINE_STACK=helper_scripts/combine_stacked.sh
T1DATA=data/Task1
INPUT_HIST_SUBJECT="$T1DATA/Subject 01.xpd"
FIGOUT=figs
LISTS=datalists

# Experiment 1 file pointers
E1_ALL_LIST=$LISTS/all_exp1.txt
E1_HIST_OUT=$FIGOUT/exp1-attribute-dists.pdf
E1_HIST_LIST=$LISTS/attr_hist.txt
E1_ACC_OUT=$FIGOUT/exp1-accuracy.pdf
E1_ACC_LIST=$LISTS/exp1_acc_gt_80.txt
E1_SORT_ACC_LIST=$LISTS/exp1_sorted_and_acc.txt
E1_SURV_OUT=$FIGOUT/exp1-survivor.pdf
E1_SURV_OUT_SUPP=$FIGOUT/exp1-survivor_supp.pdf
E1_SIC_OUT=$FIGOUT/exp1-sic.pdf
E1_FILTER_OUT=$LISTS/exp1_noviolations.txt
E1_FILTER_VIS=$LISTS/visual_drop_exp1.txt
E1_FILTER_OUT_RED=$LISTS/exp1_violations.txt
E1_KSTEST_OUT=$FIGOUT/exp1_kstest.tex
# Experiment 2 file pointers
E2_ALL_LIST=$LISTS/all_exp2.txt
E2_SPLITS=Display.Greyed,Display.Absent
E2_ACC_OUT=$FIGOUT/exp2-accuracy.pdf
E2_ACC_LIST=$LISTS/exp2_acc_gt_80.txt
E2_ACC_LIST_R=$LISTS/exp2_acc_gt_80_greyed.txt
E2_ACC_LIST_A=$LISTS/exp2_acc_gt_80_absent.txt
E2_SORT_ACC_LIST_R=$LISTS/exp2_sorted_and_acc_relative.txt
E2_SORT_ACC_LIST_A=$LISTS/exp2_sorted_and_acc_absolute.txt
E2_SURV_OUT=$FIGOUT/exp2-survivor.pdf
E2_SURV_OUT_SUPP=$FIGOUT/exp2-survivor-supp.pdf
E2_SIC_OUT=$FIGOUT/exp2-sic.pdf
E2_FILTER_OUT_R=$LISTS/exp2_noviolations_relative.txt
E2_FILTER_OUT_A=$LISTS/exp2_noviolations_absolute.txt
E2_FILTER_VIS_A=$LISTS/visual_drop_ex2_abs.txt
E2_FILTER_VIS_R=$LISTS/visual_drop_ex2_rel.txt
E2_FILTER_OUT_R_RED=$LISTS/exp2_violations_relative.txt
E2_FILTER_OUT_A_RED=$LISTS/exp2_violations_absolute.txt
E2_KSTEST_OUT_R=$FIGOUT/exp2_kstest_relative.tex
E2_KSTEST_OUT_A=$FIGOUT/exp2_kstest_absolute.tex

# Experiment 1 Figure generation
echo "Exp1 Attr Dists"
./analysis.py -i $E1_HIST_LIST hists -f tmp.pdf
mv 1tmp.pdf "$E1_HIST_OUT"

echo "Exp1 Accuracy"
./analysis.py -i "$E1_ALL_LIST" accgraph -c 80 -f "$E1_ACC_OUT" -o "$E1_ACC_LIST"
echo "Exp1 Sort"
cat "$E1_ACC_LIST" | sort > "$E1_SORT_ACC_LIST"
echo "Exp1 Filter"
./analysis.py -i "$E1_SORT_ACC_LIST" makefilter -c NoViolations > "$E1_FILTER_OUT"
./analysis.py -i "$E1_SORT_ACC_LIST" makefilter -c Violations > "$E1_FILTER_OUT_RED"
./analysis.py -i "$E1_FILTER_OUT" makefilter -c File --filterfile "$E1_FILTER_VIS" > tmpfilter.txt
mv tmpfilter.txt "$E1_FILTER_OUT"
echo "Exp1 Survivor"
./analysis.py -i "$E1_FILTER_OUT" surfacet -f "$E1_SURV_OUT" -l 0.95,0.05 -c 4
./analysis.py -i "$E1_FILTER_OUT_RED" -i "$E1_FILTER_VIS" surfacet -f "$E1_SURV_OUT_SUPP" -c 4 -l 0.95,0.05 
echo "Exp1 SIC"
./analysis.py -i "$E1_FILTER_OUT" plotsic -f "$E1_SIC_OUT" -c 4
echo "Exp1 KSTable"
./analysis.py -i "$E1_SORT_ACC_LIST" kstests
python process.table.py kstests.tex $E1_KSTEST_OUT

# Experiment 2 Figure generation
echo "Exp2 Accuracy"
./analysis.py -i "$E2_ALL_LIST" -s "$E2_SPLITS" accgraph -c 80 -f "$E2_ACC_OUT" -o "$E2_ACC_LIST"
echo "Exp2 Sorting"
cat "$E2_ACC_LIST_A" | sort > "$E2_SORT_ACC_LIST_A"
cat "$E2_ACC_LIST_R" | sort > "$E2_SORT_ACC_LIST_R"
echo "Exp2 Filter"
./analysis.py -i "$E2_SORT_ACC_LIST_R" -s Display.Greyed makefilter -c NoViolations > "$E2_FILTER_OUT_R"
./analysis.py -i "$E2_SORT_ACC_LIST_A" -s Display.Absent makefilter -c NoViolations > "$E2_FILTER_OUT_A"
./analysis.py -i "$E2_SORT_ACC_LIST_R" -s Display.Greyed makefilter -c Violations > "$E2_FILTER_OUT_R_RED"
./analysis.py -i "$E2_SORT_ACC_LIST_A" -s Display.Absent makefilter -c Violations > "$E2_FILTER_OUT_A_RED"
./analysis.py -i "$E2_FILTER_OUT_R" makefilter -c File --filterfile "$E2_FILTER_VIS_R" > tmpfilter.txt
mv tmpfilter.txt "$E2_FILTER_OUT_R"
./analysis.py -i "$E2_FILTER_OUT_A" makefilter -c File --filterfile "$E2_FILTER_VIS_A" > tmpfilter.txt
mv tmpfilter.txt "$E2_FILTER_OUT_A"
echo "Exp2 Survivor"
./analysis.py -i "$E2_FILTER_OUT_R" -s Display.Greyed surfacet -f tmp.pdf -l 0.95,0.05 -c 4
./analysis.py -i "$E2_FILTER_OUT_A" -s Display.Absent surfacet -f tmp.pdf --legendoff -l 0.95,0.05 -c 4
$COMBINE Absenttmp.pdf Greyedtmp.pdf "$E2_SURV_OUT"
echo "Exp2 Supplementary survivor"
./analysis.py -i "$E2_FILTER_OUT_R_RED" -i "$E2_FILTER_VIS_R" -s Display.Greyed surfacet -f tmp.pdf --legendoff -c 4 -t "Relative"
./analysis.py -i "$E2_FILTER_OUT_A_RED" -i "$E2_FILTER_VIS_A" -s Display.Absent surfacet -f tmp.pdf -l 0.95,0.05 -c 4 -t "Absolute"
$COMBINE Absenttmp.pdf Greyedtmp.pdf "$E2_SURV_OUT_SUPP"
echo "Exp2 SIC"
./analysis.py -i "$E2_FILTER_OUT_R" -s Display.Greyed plotsic -f test.pdf -l 0.95,0.05 -c 4
./analysis.py -i "$E2_FILTER_OUT_A" -s Display.Absent plotsic -f test.pdf -l 0.95,0.05 -c 4
$COMBINE Absenttest.pdf Greyedtest.pdf $E2_SIC_OUT

rm Absenttest.pdf Greyedtest.pdf 
