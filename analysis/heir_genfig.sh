#!/bin/bash
COMBINE_STACK=helper_scripts/combine_stacked.sh
FIGOUT=figs

echo "Stacked Bar"
./stacked_bar.py --infiles Heirarchical/Results/ModelPredictionsExp1.csv --outfile $FIGOUT/HeirarchicalExp1.pdf --titles "Experiment 1" --prior
./stacked_bar.py --infiles Heirarchical/ResultsAbsolute/ModelPredictionsExp2.csv --outfile $FIGOUT/HeirarchicalExp2.pdf --titles "Absolute" --infiles Heirarchical/ResultsRelative/ModelPredictionsExp2.csv --titles "Relative" --sortcol "MIC > 0" --column-order "MIC > 0,MIC = 0,MIC < 0"
