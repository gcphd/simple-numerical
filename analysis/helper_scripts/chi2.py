# coding: utf-8
import pandas as pd
from expyriment.misc.data_preprocessing import read_datafile
import os
from analysis import clean_info
from scipy.stats import chisquare
all_files = [os.path.abspath(os.path.join('data', 'Task1', f)) for f in os.listdir('data/Task1')]
with open('datalists/exp1_acc_gt_80.txt') as acc:
    survived = [l.strip() for l in acc.readlines()]
            
tally = {'Accept': 0, 'Reject': 0}
for f in all_files:
    data, headers, subject_info, cmts = read_datafile(f)
    tally[clean_info(subject_info)['AcceptRejectFocus']] += 1
    
print('Initial numbers of participants: {}'.format(tally))

tally_s = {'Accept': 0, 'Reject': 0}
for f in survived:
    data, headers, subject_info, cmts = read_datafile(f)
    tally_s[clean_info(subject_info)['AcceptRejectFocus']] += 1

print('Survived numbers of participants: {}'.format(tally_s))

excluded = {k: tally[k]-tally_s[k] for k in tally.keys()}
print('Excluded numbers of participants: {}'.format(excluded))
chis = chisquare([v for v in excluded.values()])
print('χ₂ = {:.4f}, p-value = {:.4f}'.format(*chis))
