len(df.RT)
len(df[notHighRT].RT)
serL = df[-notLowRT].RT.sort_values().reset_index(drop=True)
serH = df[-notHighRT].RT.sort_values().reset_index(drop=True)
serM = df[notHighRT & notLowRT].RT.sort_values().reset_index(drop=True)
serM.index = len(serL)+pd.RangeIndex(len(serM))
serH.index = len(serL) + len(serM) + pd.RangeIndex(len(serH))
if len(serL) > 0:
    ax = serL.plot()
else:
    ax = None
ax = serM.plot(ax=ax)
serH.plot(ax=ax)
plt.show()
