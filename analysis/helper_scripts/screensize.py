# coding: utf-8
import numpy as np
a4 = np.array((297, 210))
a4_p = np.array((220, 150))
scrn_p = np.array((380, 210))
sym_p = np.array([40, 25])
num_p = np.array([25, 25])
rat = a4/a4_p
dist = 740
ddist = np.array([dist, dist])
sym = rat*sym_p
num = rat*num_p
scrn = rat*scrn_p
mm_to_inch = 0.0393701
print(sym, num)

def monsize(dim):
    """calc monitor size from dimensions"""
    x = dim[0]
    y = dim[1]
    return np.sqrt(x**2 + y**2)

print('Screen in mm: ', scrn)
print('Screen in inches: ', monsize(scrn*mm_to_inch))
print('Screen in deg: ', np.rad2deg(2*np.arctan2(scrn/2, ddist)))
print('Numbers in deg: ', np.rad2deg(2*np.arctan2(num/2, ddist)))
print('Symbols in deg: ', np.rad2deg(2*np.arctan2(sym/2, ddist)))
