#!/usr/bin/env python
import subprocess
import sys
import shutil

top = sys.argv[1]
bottom = sys.argv[2]
dest = sys.argv[3]

def getdim(info):
    """get current dimensions of pdf from pdfinfo output"""
    info_lines = info.stdout.decode('utf-8').splitlines()
    sizeline = None
    for line in info_lines:
        if line.startswith('Page size:'):
            sizeline = line
            break
    if sizeline:
        _, _, x, _, y, _ = sizeline.split()
        y = float(y)*2
        return '{{{x}pt,{y}pt}}'.format(x=x, y=y)
    raise RuntimeError('Page size of pdf not found')


subprocess.run(['pdftk', top, bottom, 'cat', 'output', 'tmp.pdf'])
info = subprocess.run(['pdfinfo', '-box', 'tmp.pdf'], stdout=subprocess.PIPE)
dim = getdim(info)
subprocess.run(['pdfjam', 'tmp.pdf', '--no-landscape', '--frame', 'true', '--nup', '1x2', '--papersize', dim])
shutil.move('tmp-pdfjam.pdf', dest)
