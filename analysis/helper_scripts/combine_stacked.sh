pdftk figs/Exp1.pdf figs/prior.pdf figs/Exp2Greyed.pdf figs/Exp2Absent.pdf cat output figs/test.pdf
pdfjam figs/test.pdf --no-landscape --frame true --nup 2x2
mv test-pdfjam.pdf figs/test.pdf
pdfcrop figs/test.pdf --margins '0 0 0 0' figs/test_cropped.pdf
mv figs/test_cropped.pdf figs/heirarchical-models.pdf
