#!/bin/bash
python analysis.py -i datalists/exp1_noviolations.txt rformat -o Heirarchical/exp1_input.csv
python analysis.py -i datalists/exp2_noviolations_relative.txt -s Display.Greyed rformat -o Heirarchical/exp2_relative_input.csv
python analysis.py -i datalists/exp2_noviolations_absolute.txt -s Display.Absent rformat -o Heirarchical/exp2_absolute_input.csv
cd Heirarchical
Rscript runner.R --args Exp1 Results exp1_input.csv > Results/run.Rout 2>&1
Rscript runner.R --args Exp2 ResultsRelative exp2_relative_input.csv > ResultsRelative/run.Rout 2>&1
Rscript runner.R --args Exp2 ResultsAbsolute exp2_absolute_input.csv > ResultsAbsolute/run.Rout 2>&1
cd ..
