Survivor

________________________
|                       |
| Pass (didn't violate) |  (ie Green and White from tables)
|                       |
-------------------------
|                       |
| Fail                  | (Red from tables)
|                       |
|_______________________|

|
|
|
v

SIC
_________________________
|                       |
| Just passing          | Green and White only
|                       |
|_______________________|



SIC plots:

|        MIC = x
|   _    D+ = 
|  / \   D- = 
|./. .\. ._._._. .
|/     \_/
|
|_________________


D statistic is conservative, can use p value < 0.33 (*), < .15(**), < 0.5(***) (See note from Paul in email)
KS Tests, reference uses p value < .15 (See email from Paul)
MIC, paul also uses p value < 0.33, but never as sole evidence, not greatly justified, always use as indicative and with other evidence.

Heirarchical MIC, Bayesian SIC
