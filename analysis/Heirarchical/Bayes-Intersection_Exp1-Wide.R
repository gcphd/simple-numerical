rm(list=ls()) # Clear workspace
#Get arguments
my_args = commandArgs(trailingOnly=TRUE)[-1]
Version = my_args[1]
Folder = my_args[2]
input = my_args[3]
## Import Libraries
library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
source("sft2stan.R", local=TRUE)

print(Version)
print(Folder)
print(input)

## Read and set up Data
IntersectionData<-read.csv(input)

IntersectionBayes <- sft2stan(IntersectionData, DUMP = TRUE) # Convert data to Stan format
## Generate good starting values
source("mictest_e_simple_consumer.R", local=TRUE) # source the inits function
source("sftStanData.Rdmp") # add variables to workspace for init file to access
inits1 <- initsstan_optim2d() # Generate initial parameters
inits2 <- initsstan_optim2d() # Generate initial parameters
inits3 <- initsstan_optim2d() # Generate initial parameters
dump('inits1', file=paste(Folder, "/inits1.Rdmp", sep=""))
dump('inits2', file=paste(Folder, "/inits2.Rdmp", sep=""))
dump('inits3', file=paste(Folder, "/inits3.Rdmp", sep=""))
## Run actual Model Fit using specified arguments
Intersection.post <- stan(  file= 'micmodel_optim2e_Intersection.stan', 
  data=IntersectionBayes,
  chains = 3,             # number of Markov chains
  warmup = 10000,         # number of warmup iterations per chain
  iter = 30000,          # total number of iterations per chain
  #cores = 4,              # number of cores 
  refresh = 100,           # show progress every 'refresh' iterations
  thin = 10,              # keep every x samples
  sample_file = paste(Folder, "/Intersection_Wide_samples_c.csv"),
  diagnostic_file = paste(Folder, "/diagnostic_wide_c.csv"),
  control = list(adapt_delta = 0.85, adapt_engaged=1,max_treedepth=18, stepsize = 0.01),  # adapt_delta controls step size, bigger number == smaller steps
  init = list(inits1, inits2, inits3)
  )
options(digits=2)
options(scipen=999)
#list_of_draws <- extract(intersection2.and.post) # returns a list with named components corresponding to the model parameters.
#print(names(list_of_draws)) # Print parameter names
sumvar<-summary(Intersection.post, pars = c("modelProb_group", "modelProb_subj"), probs = c())
sumvar$summary
#get_adaptation_info(intersection2.and.post)
#setwd("C:/Users/Zach/Documents/")
filename<-paste(Folder, "/Intersection-Wide-", format(Sys.time(), "%Y-%m-%d %H--%M--%S"), ".RData", sep="")
save.image(file=filename)
