#!/bin/bash
echo "This will remove all data (input and output) for Heirarchical modelling"
read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    rm exp*.csv
	rm sftStanData.Rdmp
	find Results/. -type f -delete
	find ResultsAbsolute/. -type f -delete
	find ResultsRelative/. -type f -delete
fi
