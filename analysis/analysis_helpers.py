# Built in modules
from contextlib import contextmanager
import functools
from itertools import product, zip_longest
from math import exp
import os
import sys
# External libs
import click
import numpy as np
import statsmodels.api as sm
# Internal modules
import analysis_settings as aset


@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


sys.path.append(os.path.abspath("../Numerical/"))


class InputSpec():
    """Input Specification for an analysis click command"""
    def __init__(self, splits=None, files=None):
        '''splits is a split dictionary with values for keys and column
        / value pairs for the dictionary values'''
        self.splits = splits
        self.files = files

    def split_file_tuples(self):
        """return all splits and filenames as tuples (product of split keys and filenames)"""
        if self.splits:
            yield from product(self.splits.values(), self.files)
        else:
            yield from zip_longest([None], self.files)

    def key_file_tuples(self):
        """return all splits and filenames as tuples (product of split keys and filenames)"""
        if self.splits:
            yield from product(self.splits, self.files)
        else:
            yield from zip_longest([None], self.files)

    def key_filelist_tuples(self):
        """return all splits and a list of filenames as tuples (splitkey, filelist)"""
        if self.splits:
            for key in self.splits:
                yield key, self.files
        else:
            yield None, self.files

    def key_split_filelist_tuples(self):
        """return all splits (key and split) and a list of filenames as tuples:>
        (splitkey, split, filelist)"""
        if self.splits:
            for key in self.splits:
                yield key, self.splits[key], self.files
        else:
            yield None, None, self.files

    @property
    def num_splits(self):
        """Return number of splits passed in, or 1 if no splits"""
        return len(self.splits) if self.splits else 1


pass_input = click.make_pass_decorator(InputSpec)


def xstr(s):
    """return empty string for None else result of str(s)"""
    return '' if s is None else str(s)


def infer(ext):
    """Infer a filetype from it's extension"""
    if 'xlsx' in ext:
        return 'excel'
    elif 'htm' in ext:
        return 'html'
    elif 'txt' in ext:
        return 'plain'
    elif 'tex' in ext:
        return 'latex'
    raise RuntimeError('Unable to determine expected filetype from the extension, please specify using a valid value for outformat')


class Statistic():
    """A class representing a test statistic results"""
    def __init__(self, statistic, pvalue, fullname=None, shortname=None):
        super().__init__()
        self.statistic = statistic
        self.pvalue = pvalue
        self.alphas = []
        self._fullname = __class__.__name__ if fullname is None else fullname
        self._shortname = self._fullname if shortname is None else shortname

    def __str__(self):
        """return str representation"""
        stat_repr = '${name}$: {stat:.2f} ({pval:.2f}{p_repr})'
        p_repr = ''  # Representation (*, ** etc) of the pvalue
        if self.alphas:
            for alpha, alpha_repr in self.alphas:
                if self.pvalue < alpha:
                    p_repr = alpha_repr
                    break
        return stat_repr.format(name=self._shortname, stat=self.statistic,
                                pval=self.pvalue, p_repr=p_repr)

    def add_alpha(self, alpha=0.05, rep='*'):
        """Set the alpha value for the statistic, changes repr"""
        self.alphas.append((alpha, rep))
        self.alphas.sort()


def sic_tests(F):
    """run sic tests on a survivor class instance and return as a tuple"""
    Dplus = max(F.SIC)
    Dplus_pval = exp(-2 * F.N * Dplus**2)
    Dplus = Statistic(Dplus, Dplus_pval, fullname='Dplus', shortname='D^+')
    Dminus = min(F.SIC)
    Dminus_pval = exp(-2 * F.N * Dminus**2)
    Dminus = Statistic(Dminus, Dminus_pval, fullname='Dminus', shortname='D^-')
    return Dplus, Dminus


class Survivor():
    '''calculates individual survivors and SIC, expects a dataframe (pandas)
    with a `Category` column matching HH, HL, LH and LL and an `RT`
    column with float values for reaction times'''
    def __init__(self, df, nsamples=1000, minRT=None, maxRT=None):
        super().__init__()
        channels = aset.categories
        self.samples = dict()
        self.ecdfs = dict()
        allrts = df[df['Category'].isin(channels)]['RT']
        for cat in aset.categories:
            sample = df[df['Category'] == cat]['RT']
            self.samples[cat] = sample
            self.ecdfs[cat] = sm.distributions.ECDF(sample)
        minsamp = min(allrts) if minRT is None else minRT
        maxsamp = max(allrts) if maxRT is None else maxRT
        self.x = np.linspace(minsamp, maxsamp, num=nsamples)
        self.nsamples = nsamples

    def __getattr__(self, name):
        """If one of catgories return computed ECDF, else use super"""
        if name in aset.categories:
            return self.ecdfs[name](self.x)
        raise AttributeError

    @property
    @functools.lru_cache()
    def SIC(self):
        """Return a realised Survivor Interaction Contrast"""
        return (self.LH - self.LL) - (self.HH - self.HL)

    @property
    @functools.lru_cache()
    def max_sample_length(self):
        """return the maximum length of any cell sample"""
        return max(len(sample) for sample in self.samples.values())

    @property
    @functools.lru_cache()
    def N(self):
        """Return the value for N for the SIC"""
        N = sum(1 / len(self.samples[cat]) for cat in aset.categories)
        return 1 / N

    def rsample(self, category):
        """Return a random sample from the distribution for the category"""
        data = self.samples[category]
        return np.random.choice(data, self.nsamples)

    @property
    def rSurvivors(self):
        """Return a random sample (with replacement) of the individual
        survivors"""
        HH = self.rsample('HH')
        HL = self.rsample('HL')
        LH = self.rsample('LH')
        LL = self.rsample('LL')
        return HH, HL, LH, LL

    @functools.lru_cache()
    def stderr(self, boot_samples=1000):
        """Get stderr for the SIC"""
        bootstrap = np.full([boot_samples, 4, self.nsamples], np.nan)
        for n in range(boot_samples):
            for c, random_survivor in enumerate(self.rSurvivors):
                # Add right edge to linspace to get bins
                bins = np.append(self.x, self.x[-1] + self.x[-1] - self.x[-2])
                hist = np.histogram(random_survivor, bins)[0]
                cumsum = np.cumsum(hist)
                nsamps_notnan = np.tile(sum(~np.isnan(random_survivor)), self.nsamples)
                bootstrap[n, c, :] = 1 - cumsum / nsamps_notnan

        return np.std((bootstrap[:, 2, :] - bootstrap[:, 3, :]) - (bootstrap[:, 0, :] - bootstrap[:, 1, :]), axis=0)


def MIC(cmeans_df, colname='meanRT'):
    """return MIC from correct_means dataframe"""
    ll = cmeans_df.ix['LL'][colname]
    lh = cmeans_df.ix['LH'][colname]
    hl = cmeans_df.ix['HL'][colname]
    hh = cmeans_df.ix['HH'][colname]
    return (ll - lh) - (hl - hh)
