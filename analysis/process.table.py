# coding: utf-8
import sys
with open(sys.argv[1]) as tab:
    tabtext = tab.read().splitlines()
    
outtext = []
preamble = r'''
\begin{table}[!htb]
\centering
\resizebox{\textwidth}{!}{%'''
outtext += preamble.splitlines()

for line in tabtext:
    endline = ''
    if '&' not in line:
        outtext.append(line)
        continue
    if line.endswith(r' \\'):
        line = line[0:-3]
        endline = r' \\'
    if 'SubjectIDs' in line:
        hdrs = [cell.strip() for cell in line.split(' & ')]
    cells = line.split(' & ')
    newcells = []
    for idx, cell in enumerate(cells):
        if '(' not in cell:
            if cell.strip() == 'SubjectIDs':
                cell = 'Participants'
            newcells.append(r'\textbf{{{0}}}'.format(cell.strip()))
            continue
        pval = float(cell[cell.find('(')+1:cell.rfind(')')])
        if pval < 0.15:
            if '>' in hdrs[idx]:
                newcells.append('\cellcolor[HTML]{32CB00}'+cell.strip())
            else:
                newcells.append('\cellcolor[HTML]{FE0000}'+cell.strip())
        else:
            newcells.append(cell.strip())
    outtext.append(' & '.join(newcells) + endline)


outtext[-1] += '%'
postamble = r'''}
\end{table}'''
outtext += postamble.splitlines()
with open(sys.argv[2], 'w') as texout:
    texout.write('\n'.join(outtext))
    
