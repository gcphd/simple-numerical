import seaborn as sns
import analysis_settings as aset
import statsmodels.api as sm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import analysis_helpers as ahelp
import analysis as an


def plot_MIC(df, ax, **kwargs):
    """plot an MIC"""
    sns.pointplot(x="PriceSalience", y="RT", hue="RatingSalience",
                  data=df, markers=["o", "x"], linestyles=["-", "--"],
                  ax=ax, order=['Low', 'High'], hue_order=['Low', 'High'],
                  dodge=True, **kwargs)


def plot_ecdfs(df, ax, survivor=True, legend=True):
    """Plot ecdfs for each category"""
    for cat in aset.categories:
        sample = df[df['Category'] == cat]['RT']
        ecdf = sm.distributions.ECDF(sample)
        x = np.linspace(min(sample), max(sample))
        y = (1 - ecdf(x)) if survivor else ecdf(x)
        ax.step(x, y, label=cat)
    if legend:
        ax.legend()


def plot_SIC(F, ax, legend=True, stderr=False, style='steps'):
    """Plot SIC from survivor object"""
    if style == 'steps':
        snsargs = dict(drawstyle='steps-pre')
        mplargs = dict(step='pre')
    else:
        snsargs = dict()
        mplargs = dict()
    sns.lineplot(x=F.x, y=F.SIC, ax=ax, **snsargs)
    if stderr:
        lower = F.SIC - F.stderr()
        upper = F.SIC + F.stderr()
        sns.lineplot(x=F.x, y=lower, ax=ax, **snsargs)
        sns.lineplot(x=F.x, y=upper, ax=ax, **snsargs)
        for line in ax.lines[1:]:
            line.set_color('grey')
            line.set_linewidth(0.5)
            line.set_alpha(0.5)
        ax.fill_between(F.x, upper, lower, color='grey', alpha=0.3, **mplargs)

    ax.axhline(color='grey', alpha=0.7, linestyle='dotted')
    if legend:
        ax.legend()


def plot_distplots(df, ax):
    """Plot seaborn distplots for each category"""
    for cat in aset.categories:
        sns.distplot(df[df['Category'] == cat]['RT'], label=cat)
    ax.legend()


def plot_adjacent(fourq, info):
    """Display our four main adjacency comparisons"""
    fig, axes = plt.subplots(ncols=2, nrows=2)
    axes[0][0].set_title("MIC Greyed")
    axes[0][1].set_title("MIC Absent")
    axes[1][0].set_title('Survivor Greyed')
    axes[1][1].set_title('Survivor Absent')
    plot_MIC(fourq[fourq.Display == 'Greyed'], axes[0][0])
    plot_MIC(fourq[fourq.Display == 'Absent'], axes[0][1])
    plot_ecdfs(fourq[fourq.Display == 'Greyed'], axes[1][0])
    plot_ecdfs(fourq[fourq.Display == 'Absent'], axes[1][1])
    plt.suptitle(aset.title.format(**info))
    plt.subplots_adjust(hspace=0.4)
    return fig, axes


def plot_fourways(fourq, info, showstd):
    """Display our four main plots on 2x2 axes"""
    fig, axes = plt.subplots(ncols=2, nrows=2)
    axes[0][0].set_title("MIC")
    axes[0][1].set_title("Violin Plots")
    axes[1][0].set_title('Survivor\'s')
    axes[1][1].set_title('SIC')
    plot_MIC(fourq, axes[0][0])
    plot_ecdfs(fourq, axes[1][0])
    F = ahelp.Survivor(fourq, minRT=0)
    ax = axes[1][1]
    plot_SIC(F, ax, stderr=showstd, legend=False, style=None)
    ax.set_xlabel('RT (milliseconds)')
    yneg = min(-0.2, min(F.SIC) - 0.01) if not showstd else min(-0.2, min(F.SIC - F.stderr()) - 0.01)
    ypos = max(0.2, max(F.SIC) + 0.01) if not showstd else max(+0.2, max(F.SIC + F.stderr()) + 0.01)
    ax.set_ylim(bottom=yneg, top=ypos)
    txtbits = [str(t) for t in ahelp.sic_tests(F)]
    ax.text(0.5, 0.9, '\n'.join(txtbits), horizontalalignment='center',
            verticalalignment='center', transform=ax.transAxes,
            style='italic', size='x-small')
    sns.despine(ax=ax)
    sns.violinplot(x='Category', y='RT', data=fourq, ax=axes[0][1])
    plt.suptitle(aset.title.format(**info))
    plt.subplots_adjust(hspace=0.4)
    sns.despine()
    return fig, axes


def plot_twoways(fourq, info, showstd):
    """Display our four main plots on 2x2 axes"""
    fig, axes = plt.subplots(ncols=2, nrows=1)
    axes[0].set_title("MIC")
    axes[1].set_title("SIC")
    ax = axes[0]
    plot_MIC(fourq, ax, ci=66, errwidth=1)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda x, p: '{x:.2f}'.format(x=x / 1000.0)))
    pvals = [p for p in an.anova(fourq)['PR(>F)'][:-1]]
    txtbits = [(l, an.ratescale(p)) for l, p in zip(['p', 'r', 'p.r'], pvals)]
    txtbits = [''.join(t) for t in txtbits if t[1]]
    ax.text(1, 0.5, '\n'.join(txtbits), horizontalalignment='center',
            verticalalignment='center', transform=ax.transAxes,
            style='italic', size='x-small')
    F = ahelp.Survivor(fourq, minRT=0)
    ax = axes[1]
    plot_SIC(F, ax, stderr=showstd, legend=False, style=None)
    ax.set_xlabel('RT (milliseconds)')
    yneg = min(-0.2, min(F.SIC) - 0.01) if not showstd else min(-0.2, min(F.SIC - F.stderr()) - 0.01)
    ypos = max(0.2, max(F.SIC) + 0.01) if not showstd else max(+0.2, max(F.SIC + F.stderr()) + 0.01)
    ax.set_ylim(bottom=yneg, top=ypos)
    txtbits = [str(t) for t in ahelp.sic_tests(F)]
    ax.text(0.5, 0.9, '\n'.join(txtbits), horizontalalignment='center',
            verticalalignment='center', transform=ax.transAxes,
            style='italic', size='x-small')
    sns.despine(ax=ax)
    plt.suptitle(aset.title.format(**info))
    plt.subplots_adjust(hspace=0.4, bottom=0.4)
    sns.despine()
    return fig, axes


def plot_SICfromfourq(fourq, info, showstd):
    """Extra mods to SIC plot from outside (taken from plot_twoways)"""
    fig, ax = plt.subplots(ncols=1, nrows=1)
    ax.set_title("SIC")
    F = ahelp.Survivor(fourq, nsamples=300, minRT=0)
    plot_SIC(F, ax, stderr=showstd, style=None)
    ax.set_xlabel('RT (milliseconds)')
    yneg = min(-0.2, min(F.SIC) - 0.01) if not showstd else min(-0.2, min(F.SIC - F.stderr()) - 0.01)
    ypos = max(0.2, max(F.SIC) + 0.01) if not showstd else max(+0.2, max(F.SIC + F.stderr()) + 0.01)
    ax.set_ylim(bottom=yneg, top=ypos)
    ax.set_xlim(left=0)
    txtbits = [str(t) for t in ahelp.sic_tests(F)]
    ax.text(0.5, 0.9, '\n'.join(txtbits), horizontalalignment='center',
            verticalalignment='center', transform=ax.transAxes,
            style='italic', size='x-small')
    sns.despine(ax=ax)
    plt.suptitle(aset.title.format(**info))
    plt.subplots_adjust(hspace=0.4)
    sns.despine()
    return fig, ax


def plot_dropped(drop_df, original_length):
    """Create a plot showing the numbers of dropped trial types by drop category"""
    ax = drop_df.plot(y=['diffs', 'nan'], style=['x', 'x'])
    plt.axhline(.05 * original_length, label='Removed via .95 quantile')
    plt.axhline(drop_df.diffs.mean(), color='red', linestyle='--', label='Mean removed')
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['All trials - removed trials', 'Number of No responses'] + labels[2:])
    ax.set_xlabel('Participant')
    ax.set_ylabel('Number of trials')
