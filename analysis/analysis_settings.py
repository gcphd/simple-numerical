correct_map = {'False': 0, 'True': 1}
category_shortener = {'HighHigh': 'HH', 'HighLow': 'HL', 'LowHigh': 'LH', 'LowLow': 'LL',
                      'HighOutOfBounds': 'HO', 'LowOutOfBounds': 'LO',
                      'OutOfBoundsHigh': 'OH', 'OutOfBoundsLow': 'OL',
                      'OutOfBoundsOutOfBounds': 'OO'}
categories = 'HH HL LH LL'.split()
groups = ['PriceSalience', 'RatingSalience']
colorder = ('SubjectID;Focus;AcceptHand;FirstAttr;'
            'DoubleTargetAcc;PriceSingleTargetAcc;RatingSingleTargetAcc;DoubleDistractorAcc;'
            'Price_Anova_F;Price_Anova_P;Rating_Anova_F;Rating_Anova_P;'
            'PricexRating_Anova_F;PricexRating_Anova_P;MIC;'
            'HH_MeanRT;HL_MeanRT;LH_MeanRT;LL_MeanRT').split(';')
pairs = [('High', 'High'), ('High', 'Low'), ('Low', 'High'), ('Low', 'Low')]
title = 'Plots for {id} ({AcceptRejectFocus}/{ResponseCounterbalancing}/{PriceRatingOrder})'
titleMap = {'Greyed': 'Relative', 'Absent': 'Absolute', '': '', None: ''}
report = '''
==================================================
ANALYSIS for: {id}
--------------------------------------------------
Accept/Reject: {AcceptRejectFocus}
Accept Hand: {ResponseCounterbalancing}
Stimulus Order: {PriceRatingOrder}
--------------------------------------------------
Accuracy Data
{accuracy}
--------------------------------------------------
Correct Only RT's
{cmeans}
--------------------------------------------------
MIC: {MIC:.2f}
Anova:
{Anova}
--------------------------------------------------
KS Statistics
{ksstat}
'''

latex_report = r'''
\documentclass{{article}}
\usepackage{{graphicx}} % - Package for including images in the document.
\usepackage{{booktabs}}

\begin{{document}}
\author{{Gavin Cooper}}
\title{{Participant Data Report: \date{{\today}}}}
\maketitle

{sections}

\end{{document}}'''

section_latex = r'''
\section{{ANALYSIS for: {id}}}
Accept/Reject: {AcceptRejectFocus}\\
Accept Hand: {ResponseCounterbalancing}\\
Stimulus Order: {PriceRatingOrder}\\
\subsection{{Accuracy Data}}
{accuracy}
\subsection{{Correct Only RT's}}
{cmeans}
\subsection{{KS Statistics}}
{ksstat}
\subsection{{Figures and MIC}}
MIC: {MIC:.2f}\\
Anova:\\
{Anova}\\
\includegraphics{{{figure}}}
\newpage
'''

adjacent_section_latex = r'''
\section{{ANALYSIS for: {id}}}
Accept/Reject: {AcceptRejectFocus}\\
Stimulus Order: {PriceRatingOrder}\\
\subsection{{Accuracy Data}}
{accuracy}
\subsection{{Correct Only RT's}}
{cmeans}
\newpage
\subsection{{Figures and MIC}}
{Anova}\\
\includegraphics{{{figure}}}
'''


def places(key, numplots):
    """return legend placement based on graph type and the number of plots"""
    place_dict = {
        'survivor': {1: [(0.8, 0.05)], 2: [(0.95, 0.05), (0.95, 0.05)]}}
    return place_dict[key][numplots]
