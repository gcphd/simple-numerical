#!/usr/bin/env python
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import click
sns.set()
sns.set_style("white")

default_col_order = ['MIC = 0', 'MIC > 0', 'MIC < 0']


def mm2inch(*tupl):
    """Convert mm to inches for matplotlib figsize"""
    inch = 25.4
    if len(tupl) == 1:
        return tupl[0] / inch
    return tuple(i / inch for i in tupl)


def plot_prior(ax, equal=0.4, greater=0.4, less=0.2, col_order=None):
    """Plot a prior for the Heirarchical MIC for SFT"""
    assert (greater + less + equal) == 1.0
    if col_order is None:
        col_order = default_col_order
    prior = pd.DataFrame([{'Subject': 'Prior',
                           'MIC = 0': equal,
                           'MIC > 0': greater,
                           'MIC < 0': less}])
    (prior.sort_values('MIC = 0')
          .set_index('Subject')[col_order]
          .plot(kind='bar', stacked=True, ax=ax, rot=0, legend=False))
    ax.set_xlabel("")


def plot_model(ax, df, title, sort_col='MIC = 0', col_order=None):
    """Plot the df for heirarchical and return"""
    if col_order is None:
        col_order = default_col_order
    ax.set_title(title)
    return (df.sort_values(sort_col)
              .set_index('Subject')[col_order]
              .plot(kind='bar', stacked=True, ax=ax, rot=0, legend=False))


def data_to_plot(infile, row_axes, title, **kwargs):
    """docstring for data_to_plot"""
    posterior_pred = (pd.read_csv(infile)
                        .rename(columns={'Unnamed: 0': 'Subject'})
                        .rename(columns={'==0': 'MIC = 0',
                                         '>0': 'MIC > 0',
                                         '<0': 'MIC < 0'}))
    group_selector = posterior_pred.Subject == 'Group'
    group = posterior_pred[group_selector]
    all_subjects = posterior_pred[-group_selector]
    plot_model(row_axes[0], all_subjects, title, **kwargs)
    plot_model(row_axes[1], group, '', **kwargs).legend(loc='upper left',
                                                        bbox_to_anchor=(0, 0.3),
                                                        edgecolor='black',
                                                        frameon=True)
    row_axes[1].set_xlabel("")


@click.command()
@click.option('--infiles', type=click.Path(exists=True), multiple=True)
@click.option('--outfile', type=click.Path())
@click.option('--titles', multiple=True)
@click.option('--prior/--no-prior', default=False)
@click.option('--sortcol')
@click.option('--column-order')
def plot_models(infiles, outfile, titles, prior, sortcol, column_order):
    """plot model posteriors"""
    kwargs = {}
    if sortcol is not None:
        kwargs['sort_col'] = sortcol
    if column_order is not None:
        col_order = column_order.split(',')
        kwargs['col_order'] = col_order
        palette = sns.color_palette()
        for i, col in enumerate(col_order):
            if col == default_col_order[i]:
                continue
            position = default_col_order.index(col)
            if position < i:
                continue
            palette[i], palette[position] = palette[position], palette[i]
        sns.set_palette(palette)

    with sns.plotting_context(context='paper'):
        assert len(infiles) == len(titles)
        ratios = [1, 6, 1] if prior else [5, 1]
        cols = 3 if prior else 2
        rows = len(infiles)
        fig, axes = plt.subplots(nrows=rows,
                                 ncols=cols,
                                 figsize=mm2inch(240, rows * 100),
                                 gridspec_kw={'width_ratios': ratios},
                                 dpi=300)
        if len(axes.shape) == 1:
            axes = axes.reshape((1, -1))
        for infile, title, row_axes in zip(infiles, titles, axes):
            if prior:
                plot_prior(row_axes[0])
                row_axes = row_axes[1:]
            data_to_plot(infile, row_axes, title, **kwargs)
        if not outfile:
            plt.show()
            return
        plt.tight_layout()
        plt.savefig(outfile)
        plt.close()


if __name__ == "__main__":
    plot_models()
