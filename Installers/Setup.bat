ECHO Setting up virtualenvs etc

py -3 -m venv .\venv

call Task\venv\Scripts\Activate.bat

pip install  -r .\requirements.txt

call deactivate
