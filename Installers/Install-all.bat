@echo off
setlocal
cls

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==64BIT echo This is a 64bit operating system
ECHO Installing Python 3.6

if %OS%==32BIT .\python-3.6.8.exe /passive InstallLauncherAllUsers=0 PrependPath=1
if %OS%==64BIT .\python-3.6.8-amd64.exe /passive InstallLauncherAllUsers=0 PrependPath=1

ECHO You should now run Setup.bat...

pause
