# SFT analysis of simple consumer choice decisions

A set of code for experiments and analysis related to the SFT of consumer choice decisions

## Install

Windows includes a batch file for installation and running the task.

It requires the Installers directory to contain the following installers:

	Installers/python-3.6.5.exe

These files were available from https://www.python.org/

The task needs the DejaVu Sans font, which should be placed in the following location.

	Task/DejaVuSans.ttf

This file is part of a set of fonts available from: https://dejavu-fonts.github.io/
