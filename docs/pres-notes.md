# Honours task notes

Reject condition should have OR rule to mirror numbers of accept/reject stimuli

# Meetings with Guy

* Wording on instructions should mention accept and reject rule
* trial timing, penalty if time out?

# Presentation to group

* Order of price/rating randomised with task
* Ratio of accept/reject reduced from 80:20 to 70:30, 66:34?
* Demographic data (Handedness, Gender, Age, ...)

# Keelin testing

* Response buttons (Unacceptable/Acceptable L/R
* If no decision it moves onto next trial


# Final changes

* ~~Switch and and or for accept/reject.~~
* ~~Use reject rather than unacceptable.~~
* ~~Penalty of ~2 seconds if no response~~
* ~~Instructions fast judgements not snap judgements.~~
* ~~Tree order for BWS ends in Accept/Reject (change most regularly)~~
* ~~Add? BWS for Price/Rating order~~
* ~~Ratio of accept/reject reduced from 80:20 to 66:34~~

# Final final changes
* Install script for Keelin
* Data analysis script (cleanliness etc)
* ~~Minimum reading time for instructions~~
* ~~Final thank you screen~~
* ~~Faster than 300ms than get additional time~~
