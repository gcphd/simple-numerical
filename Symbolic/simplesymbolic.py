# -*- coding: utf-8 -*-
from expyriment import design, control, stimuli
import settings as sett
import calculations as calc
import sys
import logging

if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if arg == 'DEBUG':
            sett.debug = True
            logging.basicConfig(level=logging.DEBUG)

exp = design.Experiment(name="Simple Hotel")
control.initialize(exp)

# Define and preload standard stimuli
fixcross = stimuli.FixCross()
fixcross.preload()

tick = stimuli.TextLine('✔', text_colour=(0, 255, 0), **sett.text)
tick.preload()
cross = stimuli.TextLine('✘', text_colour=(255, 0, 0), **sett.text)
cross.preload()

# Generate the design of the experiment
prac_block = design.Block(name="Practice Block")
calc.add_trials(prac_block, sett.prac_multiplier, calc.build_single_trial)
exp.add_block(prac_block)
for b in range(sett.num_blocks):
    if b == sett.num_blocks/2:
        prac_block = design.Block(name="Practice Block")
        calc.add_trials(prac_block, sett.prac_multiplier, calc.build_single_trial)
        exp.add_block(prac_block)
    block = design.Block(name="Block{}".format(b + 1))
    calc.add_trials(block, sett.multiplier, calc.build_single_trial)
    exp.add_block(block)

exp.add_bws_factor("PriceRatingOrder", ['PriceFirst', 'RatingFirst'])
possible_responses = ['LEFT (Z)', 'RIGHT (/)']
exp.add_bws_factor("AcceptRejectFocus", ['Accept', 'Reject'])
exp.add_bws_factor("GreyedItemDisplay", ['Greyed', 'Absent'])
exp.data_variable_names = ['BlockName', 'Display', 'TrialID', 'PriceSalience',
                           'RatingSalience', 'Price', 'Rating', 'Button', 'RT',
                           'Correct', 'Timestamp']

if sett.debug:
    msglist = ['\n\tSubjID\tPriceRatingOrder\tGreyedItemResponse\tAcceptRejectFocus']
    for subjid in [s for s in range(1, 50)]:
        repr(subjid)

        factorvalues = [exp.get_permuted_bws_factor_condition(f, subjid) for f in
                        ['AcceptRejectFocus', 'GreyedItemDisplay', 'PriceRatingOrder']]
        msglist.append('\t' + '\t'.join([str(subjid)] + factorvalues))
    logging.debug('\n'.join(msglist))
    sett.trial_timeout=300
    sett.fixtime=100
    sett.instruction_time=50
    feedback_time = 100
    penalty_time = 20
    sett.multiplier = 2


def show_text_screen(textname, textbase, replace_dict=dict(),
                     continuekeys=[sett.space],
                     continue_text=sett.other_continue, wait_time=0):
    """Show a TextScreen (Instruction), pause and wait to continue"""
    if wait_time > 0:
        calc.show_instruction(textname, textbase, format_dict=replace_dict)
        exp.clock.wait(wait_time)
    finaltext = textbase + continue_text
    calc.show_instruction(textname, finaltext, format_dict=replace_dict)
    if sett.debug:
        exp.clock.wait(wait_time)
        return
    exp.keyboard.wait(keys=continuekeys)


control.start()

stimorder = exp.get_permuted_bws_factor_condition('PriceRatingOrder')
focus = exp.get_permuted_bws_factor_condition('AcceptRejectFocus')
hand = possible_responses[-1]
other_hand = list(set(possible_responses) -
                  set([hand]))[0]
display = exp.get_permuted_bws_factor_condition('GreyedItemDisplay')
logging.debug('Factors: %s %s %s', stimorder, focus, hand)
conjunctives = ('AND', 'OR')
if focus == 'Accept':
    pcut = sett.accept_price['cutoff']
    rcut = sett.accept_rate['cutoff']
    if stimorder == 'PriceFirst':
        stim_selector = 0
    else:
        stim_selector = 2
else:
    pcut = sett.reject_price['cutoff']
    rcut = sett.reject_rate['cutoff']
    conjunctives = conjunctives[::-1]
    if stimorder == 'PriceFirst':
        stim_selector = 1
    else:
        stim_selector = 3
grey_shift = 0 if display == 'Greyed' else 4

instrDict = dict(price=pcut, rating=rcut, reject_conjunctive=conjunctives[1],
                 accept_conjunctive=conjunctives[0], acceptkey=hand,
                 rejectkey=other_hand)
restDict = {k: v for k, v in instrDict.items()}
restDict['totalblocks'] = sett.num_blocks + 2
show_text_screen('Instruction', sett.instruction_text, replace_dict=instrDict,
                 continue_text=sett.instruction_continue,
                 wait_time=sett.instruction_time)

for b, block in enumerate(exp.blocks, start=1):
    if block.name == 'Practice Block':
        if b == 1:
            show_text_screen('Practice', sett.practice_text)
        else:
            show_text_screen('Practice', sett.next_prac_text)
            grey_shift = 4 - grey_shift
        counter = 0
    for trial in block.trials:
        fixcross.present()
        stim = trial.stimuli[stim_selector + grey_shift]
        exp.clock.wait(sett.fixtime - stim.preload())
        stim.present()
        ts = exp.clock.time
        key, rt = exp.keyboard.wait(keys=sett.trialkeys,
                                    duration=sett.trial_timeout)
        if block.name == 'Practice Block':
            if calc.classify_response(key, trial, hand, focus):
                tick.present()
                counter += 1
            else:
                cross.present()
            if sett.debug:
                exp.screen.save('{}.png'.format(trial.id))
            exp.clock.wait(sett.feedback_time)
        if key is None or rt < sett.minimum_rt:  # No response or too fast
            fixcross.present()
            exp.clock.wait(sett.penalty_time)
        exp.data.add([block.name, 'Absent' if grey_shift else 'Greyed',
                      trial.id, trial.get_factor('PriceSalience'),
                      trial.get_factor('RatingSalience'),
                      trial.get_factor(focus + 'Price'),
                      trial.get_factor(focus + 'Rating'),
                      sett.keylabel[key], rt,
                      calc.classify_response(key, trial, hand, focus), ts])
    if b != len(exp.blocks):
        if block.name == 'Practice Block':
            pcCorr = '{:.0}%'.format(float(counter) / len(block.trials))
            show_text_screen('Post Prac Break', sett.postprac_text,
                             replace_dict={'accuracy': pcCorr})
        restDict['currblock'] = b
        show_text_screen('Rest Break', sett.rest_text, replace_dict=restDict,
                         wait_time=sett.instruction_time)
        if sett.debug:
            continue

show_text_screen('Thank you', sett.thankyou_text)

control.end()
