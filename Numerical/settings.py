# -*- coding: utf-8 -*-
from expyriment import misc

# Task Settings
debug = False
rKey = misc.constants.K_SLASH
lKey = misc.constants.K_z
space = misc.constants.K_SPACE
trialkeys = [rKey, lKey]
keylabel = {rKey: 'RIGHT', lKey: 'LEFT', None: 'NORESPONSE'}

# Each subdict key (bool, bool, bool) corresponds to
# (LEFT in hand, OutOfBounds in factors, key == LEFT)
correct_map = {
    'Accept': {
        (True, True, True): False,
        (True, True, False): True,
        (True, False, True): True,
        (True, False, False): False,
        (False, True, True): True,
        (False, True, False): False,
        (False, False, True): False,
        (False, False, False): True,
    },
    'Reject': {
        (True, True, True): True,
        (True, True, False): False,
        (True, False, True): False,
        (True, False, False): True,
        (False, True, True): False,
        (False, True, False): True,
        (False, False, True): True,
        (False, False, False): False,
    },
}

num_blocks = 4
trial_timeout = 4500
fixtime = 1000
instruction_time = 5000
categories = 'HH,HL,LH,LL,HO,LO,OL,OH,OO'.split(',')
trials_per_cat = [int(x) for x in '5,5,5,5,2,2,2,2,2'.split(',')]
multiplier = 7
prac_multiplier = 1
minimum_rt = 300

feedback_time = 1000
penalty_time = 2000

basestim = dict(PriceFirst='${price:.0f}\n\n{rating:.0f}%',
                RatingFirst='{rating:.0f}%\n\n${price:.0f}')

# Text Stimulus Settings
textsize = (300, 300)
font = 'DejaVuSans.ttf'
text = dict(text_font=font, text_size=32)
textbox = dict(position=(0, -100), text_justification=1)
textbox.update(text)
screen = dict(heading_size=64, text_justification=1)
screen.update(text)
screen['text_size'] = 18
instruction_text = '''Your job will be to make fast judgements
on possible locations for executives to stay at on trips.

You should REJECT any hotel for which:
    The price is over ${price}
    {reject_conjunctive}
    The rating is less than {rating}%

This means you should ACCEPT any hotel for which:
    The price is below ${price}
    {accept_conjunctive}
    The rating is above {rating}%

If the hotel should be rejected press the {rejectkey} key.
If the hotel should be accepted press the {acceptkey} key.'''

instruction_continue = '''
You have a large number of hotels to check, so press <Space> to
get started'''

rest_text = '''You are doing well
Just remember

You should REJECT any hotel for which:
    The price is over ${price}
    {reject_conjunctive}
    The rating is less than {rating}%

This means you should ACCEPT any hotel for which:
    The price is below ${price}
    {accept_conjunctive}
    The rating is above {rating}%

If the hotel should be rejected press the {rejectkey} key.
If the hotel should be accepted press the {acceptkey} key.

You have finished {currblock} of {totalblocks} groups of hotels'''

other_continue = '''

Press <Space> to continue'''

practice_text = '''You will first do a smaller practice block'''

postprac_text = '''You achieved {accuracy} accuracy in practice'''

thankyou_text = '''Thank you for your participation

You are now finished the computer-based section of the experiment.'''

# Salience Map

salience_map = dict(H='highab', L='lowab', O='outab')
factor_map = dict(H='High', L='Low', O='OutOfBounds')
# Price Settings


accept_price = dict(cutoff=500,
                    lowab=(350, 450, 400, 25),
                    highab=(150, 250, 200, 25),
                    outab=(650, 750, 700, 25))

reject_price = dict(cutoff=500,
                    lowab=(550, 650, 600, 25),
                    highab=(750, 850, 800, 25),
                    outab=(250, 350, 300, 25))
# Rating Settings

accept_rate = dict(cutoff=50,
                   lowab=(55, 65, 60, 2.5),
                   highab=(75, 85, 80, 2.5),
                   outab=(25, 35, 30, 2.5))

reject_rate = dict(cutoff=50,
                   lowab=(35, 45, 40, 2.5),
                   highab=(15, 25, 20, 2.5),
                   outab=(65, 75, 70, 2.5))
