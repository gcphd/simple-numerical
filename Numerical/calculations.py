# -*- coding: utf-8 -*-
from scipy.stats import truncnorm
from expyriment import stimuli, design
import settings as sett
import logging

salience_map = sett.salience_map
factor_map = sett.factor_map


def spec_truncnorm(clip_start, clip_end, mean, sd):
    '''Calculate a,b for truncnorm based on clipping and mean/sd'''
    return (clip_start - mean) / sd, (clip_end - mean) / sd


# Generate the design of the experiment
def add_trials(block, multiplier, trial_builder):
    """Add multiplier lots of category trials to the block"""
    for category, number in zip(sett.categories, sett.trials_per_cat):
        for _ in range(multiplier * number):
            block.add_trial(trial_builder(category))
    block.shuffle_trials()


def classify_response(key, trial, hand, focus):
    logging.debug('Trial Factors %s', trial.factors_as_text)
    if key is None:
        return False
    mapkey = ('LEFT' in hand,
              'OutOfBounds' in trial.factors_as_text,
              key == sett.lKey)
    return sett.correct_map[focus][mapkey]


def channel_value(salience, channel_settings):
    """Return a sampled value for the salience level of the channel specified
    by the channel_settings"""
    try:
        rmin, rmax, rmean, rsd = channel_settings[salience_map[salience]]
    except KeyError as e:
        raise ValueError('Not valid value for salience') from e
    trunc_a, trunc_b = spec_truncnorm(rmin, rmax, rmean, rsd)
    return truncnorm.rvs(trunc_a, trunc_b, loc=rmean, scale=rsd)


def genStimulus(price, rating, category, order):
    """Generate a single Hotel stimulus based on a category
    category can be one of {HH, HL, LH, LL, HO, LO, OL, OH, OO}
    where H = High salience, L = Low salience, O = Outside bounds"""

    text = sett.basestim[order].format(price=price, rating=rating)
    return stimuli.TextBox(text, sett.textsize, **sett.textbox)


def asFactor(category):
    """return factors for the category position"""
    return factor_map[category[0]], factor_map[category[1]]


def generate_stimulus_values(focus, category, trial, order):
    """Generate the price/rating values for a particular focus, where
    focus can either be on accept or reject (between subjects factor"""
    if focus == 'Accept':
        price_dict = sett.accept_price
        rate_dict = sett.accept_rate
    elif focus == 'Reject':
        price_dict = sett.reject_price
        rate_dict = sett.reject_rate
    else:
        raise ValueError('Focus must be "Accept" or "Reject"')
    price = channel_value(category[0], price_dict)
    rating = channel_value(category[1], rate_dict)
    trial.set_factor(focus + 'Price', '${:.0f}'.format(price))
    trial.set_factor(focus + 'Rating', '{:.0f}%'.format(rating))
    stim = genStimulus(price, rating, category, order)
    return stim


def build_single_trial(category):
    """build a single stimulus trial based on category"""
    trial = design.Trial()
    price_salience, rating_salience = asFactor(category)
    trial.set_factor('PriceSalience', price_salience)
    trial.set_factor('RatingSalience', rating_salience)
    trial.add_stimulus(generate_stimulus_values('Accept', category, trial, 'PriceFirst'))
    trial.add_stimulus(generate_stimulus_values('Reject', category, trial, 'PriceFirst'))
    trial.add_stimulus(generate_stimulus_values('Accept', category, trial, 'RatingFirst'))
    trial.add_stimulus(generate_stimulus_values('Reject', category, trial, 'RatingFirst'))
    return trial


def show_instruction(screen_name, base_text, format_dict=dict()):
    """Build a TextScreen object using the base text and format it
    appropriately"""
    txt = base_text.format(**format_dict)
    screen = stimuli.TextScreen(screen_name, txt, **sett.screen)
    screen.present()
