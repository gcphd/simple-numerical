# -*- coding: utf-8 -*-
from expyriment import design, control, stimuli
import settings as sett
import calculations as calc
import sys
import logging

if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if arg == 'DEBUG':
            sett.debug = True
            logging.basicConfig(level=logging.DEBUG)

exp = design.Experiment(name="Simple Hotel")
control.initialize(exp)

# Define and preload standard stimuli
fixcross = stimuli.FixCross()
fixcross.preload()

tick = stimuli.TextLine('✔', text_colour=(0, 255, 0), **sett.text)
tick.preload()
cross = stimuli.TextLine('✘', text_colour=(255, 0, 0), **sett.text)
cross.preload()

# Generate the design of the experiment
prac_block = design.Block(name="Practice Block")
calc.add_trials(prac_block, sett.prac_multiplier, calc.build_single_trial)
exp.add_block(prac_block)
for b in range(sett.num_blocks):
    block = design.Block(name="Block{}".format(b + 1))
    calc.add_trials(block, sett.multiplier, calc.build_single_trial)
    exp.add_block(block)

exp.add_bws_factor("PriceRatingOrder", ['PriceFirst', 'RatingFirst'])
exp.add_bws_factor("ResponseCounterbalancing", ['LEFT (Z)', 'RIGHT (/)'])
exp.add_bws_factor("AcceptRejectFocus", ['Accept', 'Reject'])
exp.data_variable_names = ['BlockName', 'TrialID', 'PriceSalience',
                           'RatingSalience', 'Price', 'Rating', 'Button', 'RT',
                           'Correct']

if sett.debug:
    msglist = ['\n\tSubjID\tPriceRatingOrder\tResponseCounterbalancing\tAcceptRejectFocus']
    for subjid in [s for s in range(1, 17)]:
        repr(subjid)

        factorvalues = [exp.get_permuted_bws_factor_condition(f, subjid) for f in
                        ['PriceRatingOrder', 'ResponseCounterbalancing', 'AcceptRejectFocus']]
        msglist.append('\t' + '\t'.join([str(subjid)] + factorvalues))
    logging.debug('\n'.join(msglist))


def show_text_screen(textname, textbase, replace_dict=dict(),
                     continuekeys=[sett.space],
                     continue_text=sett.other_continue, wait_time=0):
    """Show a TextScreen (Instruction), pause and wait to continue"""
    if wait_time > 0:
        calc.show_instruction(textname, textbase, format_dict=replace_dict)
        exp.clock.wait(wait_time)
    finaltext = textbase + continue_text
    calc.show_instruction(textname, finaltext, format_dict=replace_dict)
    exp.keyboard.wait(keys=continuekeys)


control.start()

stimorder = exp.get_permuted_bws_factor_condition('PriceRatingOrder')
focus = exp.get_permuted_bws_factor_condition('AcceptRejectFocus')
hand = exp.get_permuted_bws_factor_condition('ResponseCounterbalancing')
other_hand = list(set(exp.get_bws_factor('ResponseCounterbalancing')) -
                  set([hand]))[0]
logging.debug('Factors: %s %s %s', stimorder, focus, hand)
conjunctives = ('AND', 'OR')
if focus == 'Accept':
    pcut = sett.accept_price['cutoff']
    rcut = sett.accept_rate['cutoff']
    if stimorder == 'PriceFirst':
        stim_selector = 0
    else:
        stim_selector = 2
else:
    pcut = sett.reject_price['cutoff']
    rcut = sett.reject_rate['cutoff']
    conjunctives = conjunctives[::-1]
    if stimorder == 'PriceFirst':
        stim_selector = 1
    else:
        stim_selector = 3

instrDict = dict(price=pcut, rating=rcut, reject_conjunctive=conjunctives[1],
                 accept_conjunctive=conjunctives[0], acceptkey=hand,
                 rejectkey=other_hand)
restDict = {k: v for k, v in instrDict.items()}
restDict['totalblocks'] = sett.num_blocks + 1
show_text_screen('Instruction', sett.instruction_text, replace_dict=instrDict,
                 continue_text=sett.instruction_continue,
                 wait_time=sett.instruction_time)

for b, block in enumerate(exp.blocks, start=1):
    if block.name == 'Practice Block':
        show_text_screen('Practice', sett.practice_text)
        counter = 0
    for trial in block.trials:
        fixcross.present()
        stim = trial.stimuli[stim_selector]
        exp.clock.wait(sett.fixtime - stim.preload())
        stim.present()
        key, rt = exp.keyboard.wait(keys=sett.trialkeys,
                                    duration=sett.trial_timeout)
        if block.name == 'Practice Block':
            if sett.debug:
                exp.screen.save('{}.png'.format(trial.id))
            if calc.classify_response(key, trial, hand, focus):
                tick.present()
                counter += 1
            else:
                cross.present()
            exp.clock.wait(sett.feedback_time)
        if key is None or rt < sett.minimum_rt:  # No response or too fast
            fixcross.present()
            exp.clock.wait(sett.penalty_time)
        exp.data.add([block.name, trial.id, trial.get_factor('PriceSalience'),
                      trial.get_factor('RatingSalience'),
                      trial.get_factor(focus + 'Price'),
                      trial.get_factor(focus + 'Rating'),
                      sett.keylabel[key], rt,
                      calc.classify_response(key, trial, hand, focus)])
    if b != len(exp.blocks):
        if block.name == 'Practice Block':
            pcCorr = '{:.0}%'.format(float(counter) / len(block.trials))
            show_text_screen('Post Prac Break', sett.postprac_text,
                             replace_dict={'accuracy': pcCorr})
        restDict['currblock'] = b
        show_text_screen('Rest Break', sett.rest_text, replace_dict=restDict,
                         wait_time=sett.instruction_time)
        exp.keyboard.wait(keys=[sett.space])

show_text_screen('Thank you', sett.thankyou_text)

control.end()
