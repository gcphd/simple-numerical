from psychopy import visual, clock, event, monitors
from settings import space, debug

# for the custom calibration/validation routine to work properly, we recommend setting screen units to "pix"
win = visual.Window(monitor=monitors.Monitor('PhDLaptop'), units='pix', fullScr=True)

# Text Stimulus Settings
font = 'DejaVuSans'
text = dict(font=font)
textbox = dict(pos=(0, 0), height=15, alignText='center')
textbox.update(text)
screen = dict(pos=(0, 150), height=15, alignText='center', anchorVert='top')
screen.update(text)
instruction_text = '''Your job will be to make fast judgements
on possible locations for executives to stay at on trips.

You should REJECT any hotel for which:
    The price is over {price} dollar signs
    {reject_conjunctive}
    The rating is less than {rating} stars

This means you should ACCEPT any hotel for which:
    The price is below {price} dollar signs
    {accept_conjunctive}
    The rating is above {rating} stars

If the hotel should be rejected press the {rejectkey} key.
If the hotel should be accepted press the {acceptkey} key.'''

instruction_continue = '''

You have a large number of hotels to check, so press <Space> to get started'''

rest_text = '''You are doing well
Just remember

You should REJECT any hotel for which:
    The price is over {price} dollar signs
    {reject_conjunctive}
    The rating is less than {rating} stars

This means you should ACCEPT any hotel for which:
    The price is below {price} dollar signs
    {accept_conjunctive}
    The rating is above {rating} stars

If the hotel should be rejected press the {rejectkey} key.
If the hotel should be accepted press the {acceptkey} key.

You have finished {currblock} of {totalblocks} groups of hotels'''

other_continue = '''

Press <Space> to continue'''

practice_text = '''You will first do a smaller practice block'''

calibration_text = '''We will now run the Eyelink calibration

C - Run calibration
V - Run validation
O - Start the experiment'''

postprac_text = '''You achieved {accuracy} accuracy in practice'''

next_prac_text = '''You will now do another practice task
The way the price and rating is displayed will change slightly,
however nothing else has changed.'''

thankyou_text = '''Thank you for your participation

You are now finished the computer-based section of the experiment.'''


fixcross = visual.ShapeStim(
    win,
    vertices=((0, -15), (0, 15), (0, 0), (-15, 0), (15, 0)),
    lineWidth=5,
    closeShape=False,
    lineColor="white"
)

tick = visual.TextStim(win, text='✔', color=(0, 1.0, 0), **text)
cross = visual.TextStim(win, '✘', color=(1.0, 0, 0), **text)
option_display = visual.TextStim(win, text='', **textbox)


def instruction(textname, textbase, replace_dict=dict(),
                continuekeys=[space],
                continue_text=other_continue, wait_time=0):
    """Show a TextScreen (Instruction), pause and wait to continue"""
    def show_instruction(screen_name, base_text, format_dict=dict()):
        """Build a TextScreen object using the base text and format it
        appropriately"""
        txt = base_text.format(**format_dict)
        txtbox = visual.TextStim(win, text=txt, name=screen_name, **screen)
        txtbox.draw()
        win.flip()

    if wait_time > 0:
        show_instruction(textname, textbase, format_dict=replace_dict)
        clock.wait(wait_time)
    finaltext = textbase + continue_text
    show_instruction(textname, finaltext, format_dict=replace_dict)
    if debug:
        clock.wait(wait_time)
        return
    event.waitKeys(maxWait=60, keyList=continuekeys)
