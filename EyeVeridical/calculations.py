# -*- coding: utf-8 -*-
from scipy.stats import truncnorm
from psychopy import visual
from functools import lru_cache
from settings import (salience_map, factor_map, dkeys, categories,
                      trials_per_cat, lKey, correct_map, basestim,
                      accept_price, accept_rate, reject_price, reject_rate)
import logging


def spec_truncnorm(clip_start, clip_end, mean, sd):
    '''Calculate a,b for truncnorm based on clipping and mean/sd'''
    return (clip_start - mean) / sd, (clip_end - mean) / sd


def expand_category(category):
    """Expand a two char category id to a 2 element dict"""
    return dict(zip(dkeys, category))


# Generate the design of the experiment
def add_trials(block, trial_builder):
    """Add multiplier lots of category trials to the block"""
    for category, number in zip(categories, trials_per_cat):
        for _ in range(number):
            block.append(trial_builder(category))


def classify_response(key, trial, hand, focus):
    logging.debug(f'Trial Details: {trial}')
    if key is None:
        return False
    mapkey = ('LEFT' in hand,
              'D' in trial.values(),
              key == lKey)
    return correct_map[focus][mapkey]


def channel_value(salience, channel_settings):
    """Return a sampled value for the salience level of the channel specified
    by the channel_settings"""
    try:
        rmin, rmax, rmean, rsd = channel_settings[salience_map[salience]]
    except KeyError as e:
        raise ValueError('Not valid value for salience') from e
    trunc_a, trunc_b = spec_truncnorm(rmin, rmax, rmean, rsd)
    return truncnorm.rvs(trunc_a, trunc_b, loc=rmean, scale=rsd)


@lru_cache(maxsize=32)
def genStimulus(price, rating, category, order):
    """Generate a single Hotel stimulus based on a category
    category can be one of {HH, HL, LH, LL, HO, LO, OL, OH, OO}
    where H = High salience, L = Low salience, O = Outside bounds"""
    text = basestim[order].format(price=price, rating=rating)
    return text


def asFactor(category):
    """return factors for the category position"""
    return factor_map[category[0]], factor_map[category[1]]


def generate_stimulus_values(focus, category, trial, order):
    """Generate the price/rating values for a particular focus, where
    focus can either be on accept or reject (between subjects factor"""
    if focus == 'Accept':
        price_dict = accept_price
        rate_dict = accept_rate
    elif focus == 'Reject':
        price_dict = reject_price
        rate_dict = reject_rate
    else:
        raise ValueError('Focus must be "Accept" or "Reject"')
    price = channel_value(category[0], price_dict)
    rating = channel_value(category[1], rate_dict)
    stim = genStimulus(price, rating, category, order)
    return stim


def build_single_trial(category):
    """build a single stimulus trial based on category"""
    trial = expand_category(category)
    trial.update(
        {
            'stimulus': [
                generate_stimulus_values('Accept', category, trial, 'PriceFirst'),
                generate_stimulus_values('Reject', category, trial, 'PriceFirst'),
                generate_stimulus_values('Accept', category, trial, 'RatingFirst'),
                generate_stimulus_values('Reject', category, trial, 'RatingFirst')
            ]
        })
    return trial
