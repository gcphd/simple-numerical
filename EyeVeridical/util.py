import json
from PyInquirer import prompt
from settings import between_subjects_factors


def edit_info(info_dict):
    '''For each key on the info_dict, enable the user to alter the values'''
    questions = []
    for key, val in info_dict.items():
        question = {}
        if key == 'SubjectNumber':
            question['type'] = 'input'
            question['message'] = f'Please enter {key} (currently {val}): '
            question['name'] = key
        else:
            question['name'] = key
            question['type'] = 'list'
            question['choices'] = between_subjects_factors[key]
            question['message'] = f'Please select value for {key}: '
        questions.append(question)

    answers = prompt(questions)
    for key, val in answers.items():
        if val != '':
            if key == 'SubjectNumber':
                info_dict[key] = f'{int(val):03}'
            else:
                info_dict[key] = val


def read_and_increment(info_dict, filename):
    '''Read an information dictionary specification from a provided json file.

    The info_dict is assumed to have a very specific format, and same goes for
    the json file. If there is any acccess error the info_dict is not edited and
    the function will silently end.

    There will be key specific changes to the values returned.'''
    try:
        with open(filename, 'r') as jsonfile:
            lastrun = json.load(jsonfile)
        info_dict.update(lastrun)
        info_dict['SubjectNumber'] = f'{lastrun["SubjectNumber"]+1:03}'  # Increment and zero pad
    except EnvironmentError:
        pass


def write_to_file(info_dict, filename):
    '''Write an information dictionary specification to a provided json file.

    The info_dict is assumed to have a very specific format. If there is any
    acccess error the info_dict is not edited and the function will silently
    end.'''
    try:
        save_dict = info_dict.copy()
        save_dict['SubjectNumber'] = int(save_dict['SubjectNumber'])
        with open(filename, 'w') as jsonfile:
            json.dump(jsonfile, save_dict)
    except EnvironmentError:
        pass


# STEP I: get subject info
# Get subject information from the experiment runner

expInfo = {
    'SubjectNumber': '000',
    'PriceRatingOrder': '',
    'AcceptRejectFocus': '',
    'ResponseCounterbalancing': ''
}

read_and_increment(expInfo, 'lastrun.json')
edit_info(expInfo)
