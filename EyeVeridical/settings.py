# -*- coding: utf-8 -*-
from psychopy import logging

# Task Settings
debug = True
rKey = 'slash'
lKey = 'z'
space = 'space'
trialkeys = [rKey, lKey]
keylabel = {rKey: 'RIGHT', lKey: 'LEFT', None: 'NORESPONSE'}
possible_responses = ['LEFT (Z)', 'RIGHT (/)']
between_subjects_factors = {
    'PriceRatingOrder': ['PriceFirst', 'RatingFirst'],
    'AcceptRejectFocus': ['Accept', 'Reject'],
    'ResponseCounterbalancing': possible_responses
}

# Each subdict key (bool, bool, bool) corresponds to
# (LEFT in hand, OutOfBounds in factors, key == LEFT)
correct_map = {
    'Accept': {
        (True, True, True): False,
        (True, True, False): True,
        (True, False, True): True,
        (True, False, False): False,
        (False, True, True): True,
        (False, True, False): False,
        (False, False, True): False,
        (False, False, False): True,
    },
    'Reject': {
        (True, True, True): True,
        (True, True, False): False,
        (True, False, True): False,
        (True, False, False): True,
        (False, True, True): False,
        (False, True, False): True,
        (False, False, True): True,
        (False, False, False): False,
    },
}

num_blocks = 4
trial_timeout = 4.5
fixation_time = 1
instruction_time = 5
categories = 'HH,HL,LH,LL,HD,LD,DL,DH,DD'.split(',')
trials_per_cat = [int(x) for x in '5,5,5,5,2,2,2,2,2'.split(',')]
dkeys = ['PriceSalience', 'QualitySalience']
multiplier = 7
prac_multiplier = 1
minimum_rt = .3

feedback_time = 1
penalty_time = 2

basestim = dict(PriceFirst='${price:.0f}\n\n{rating:.0f}%',
                RatingFirst='{rating:.0f}%\n\n${price:.0f}')

# Salience Map

salience_map = dict(H='highab', L='lowab', D='distab')
factor_map = dict(H='High', L='Low', D='Distractor')

# Price Settings

accept_price = dict(cutoff=500,
                    lowab=(350, 450, 400, 25),
                    highab=(150, 250, 200, 25),
                    distab=(650, 750, 700, 25))

reject_price = dict(cutoff=500,
                    lowab=(550, 650, 600, 25),
                    highab=(750, 850, 800, 25),
                    distab=(250, 350, 300, 25))
# Rating Settings

accept_rate = dict(cutoff=50,
                   lowab=(55, 65, 60, 2.5),
                   highab=(75, 85, 80, 2.5),
                   distab=(25, 35, 30, 2.5))

reject_rate = dict(cutoff=50,
                   lowab=(35, 45, 40, 2.5),
                   highab=(15, 25, 20, 2.5),
                   distab=(65, 75, 70, 2.5))

if debug:
    logging.console.setLevel(logging.DEBUG)
    trial_timeout = .3
    fixation_time = .2
    instruction_time = .5
    feedback_time = .1
    penalty_time = .02
    multiplier = 2
