#!/usr/bin/env python
# -*- coding: utf-8 -*-
from util import expInfo
import settings as sett
import calculations as calc
from stimuli import (fixcross, instruction, instruction_text, option_display,
                     instruction_continue, tick, cross, practice_text, win,
                     postprac_text, rest_text, thankyou_text)

from psychopy import clock, data, logging, event
# import the custom calibrarion/validation routine for Psychopy

# STEP II: Setup the experiment handler which will handle data output etc
exp = data.ExperimentHandler(name='EyeVeridical',
                             version='0.2',
                             extraInfo=expInfo,
                             dataFileName='data/EV' + expInfo['SubjectNumber'])

# STEP III: Setup the practice trials
trial_types = []
calc.add_trials(trial_types, calc.build_single_trial)
practice = data.TrialHandler(trialList=trial_types, nReps=sett.prac_multiplier, name='block', extraInfo={'blockname': 'practice'})
exp.addLoop(practice)

# STEP IV: Setup the main blocks
for blocknum in range(sett.num_blocks):
    trial_types = []
    calc.add_trials(trial_types, calc.build_single_trial)
    block = data.TrialHandler(trialList=trial_types, nReps=sett.multiplier, name='block', extraInfo={'blockname': f'block_{blocknum+1}'})
    exp.addLoop(block)

# STEP V: Prepare for text screens
stimorder = expInfo['PriceRatingOrder']
focus = expInfo['AcceptRejectFocus']
hand = expInfo['ResponseCounterbalancing']
other_hand = list(set(sett.possible_responses) - set([hand]))[0]
logging.debug(f'Factors: {stimorder} {focus} {hand}')
conjunctives = ('AND', 'OR')
if focus == 'Accept':
    pcut = sett.accept_price['cutoff']
    rcut = sett.accept_rate['cutoff']
    if stimorder == 'PriceFirst':
        stim_selector = 0
    else:
        stim_selector = 2
else:
    pcut = sett.reject_price['cutoff']
    rcut = sett.reject_rate['cutoff']
    conjunctives = conjunctives[::-1]
    if stimorder == 'PriceFirst':
        stim_selector = 1
    else:
        stim_selector = 3
instrDict = dict(price=pcut, rating=rcut, reject_conjunctive=conjunctives[0],
                 accept_conjunctive=conjunctives[0], acceptkey=hand,
                 rejectkey=other_hand)


# STEP VI: Define a trial
def show_trial(trial, block):
    """Present a trial"""
    fixcross.draw()
    win.flip()
    fixtime = clock.getTime()
    option_display.text = trial['stimulus'][stim_selector]
    option_display.draw()
    clock.wait(sett.fixation_time - (clock.getTime() - fixtime))
    win.flip()
    ts = clock.getTime()
    response = event.waitKeys(maxWait=sett.trial_timeout,
                              keyList=sett.trialkeys,
                              timeStamped=True)
    if response is None:
        key, rt = None, None
    else:
        key, rt = response[0]
        rt = rt - ts

    if block.extraInfo['blockname'] == 'practice':
        if calc.classify_response(key, trial, hand, focus):
            tick.draw()
        else:
            cross.draw()
        win.flip()
        clock.wait(sett.feedback_time)
    if key is None or rt < sett.minimum_rt:  # No response or too fast
        fixcross.draw()
        win.flip()
        clock.wait(sett.penalty_time)
    block.addData('response.key', key)
    block.addData('response.time', rt)
    block.addData('response.correct', calc.classify_response(key, trial, hand, focus))


# STEP VII: run all trials
instruction('Instruction', instruction_text, replace_dict=instrDict,
            continue_text=instruction_continue, wait_time=sett.instruction_time)

restDict = instrDict.copy()
restDict['totalblocks'] = sett.num_blocks + 1

instruction('Practice', practice_text)

for trial in practice:
    practice.addData('blockname', 'practice')
    show_trial(trial, practice)
    exp.nextEntry()

pcCorr = '{:.0}%'.format(0.33)
instruction('Post Prac Break', postprac_text, replace_dict={'accuracy': pcCorr})

for n, block in enumerate(exp.loopsUnfinished, 1):
    for thisTrial in block:
        block.addData('blockname', f'block_{n}')
        show_trial(thisTrial, block)
        exp.nextEntry()
    restDict['currblock'] = n
    instruction('Rest Break', rest_text, replace_dict=restDict,
                wait_time=sett.instruction_time)

instruction('Thank you', thankyou_text)
print("Done. 'exp' experimentHandler will now (end of script) save data to testExp.csv")
print(" and also to testExp.psydat, which is a pickled version of `exp`")
